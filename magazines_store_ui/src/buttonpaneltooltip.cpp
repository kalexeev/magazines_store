#include "buttonpaneltooltip.h"

#include <QPainter>

#include "drawutils.h"

ButtonPanelTooltip::ButtonPanelTooltip(QWidget *parent)
	: QWidget(parent)
{
}

ButtonPanelTooltip::~ButtonPanelTooltip()
{

}

void ButtonPanelTooltip::paintEvent( QPaintEvent * )
{
	QPainter p(this);

	QRect rectSelection = rect().adjusted(1,1,-1,-1);

	QPainterPath path;
	DrawUtils::addArrowRect(path, rectSelection, 3, 3);

	p.setRenderHint(QPainter::Antialiasing);
	p.setPen(QPen(QColor(102,91,54), 1));
	p.setBrush(QColor(237,215,140,209));
	p.drawPath(path);
	p.setRenderHint(QPainter::Antialiasing,false);
	
	p.setPen(QPen(QColor(79,70,40), 1));
	p.setFont(font());

	p.drawText( rect().adjusted(rect().height()/2,0,0,0), Qt::AlignLeft | Qt::AlignVCenter, _text );
}

void ButtonPanelTooltip::setText( const QString& text )
{
	int w = this->fontMetrics().width(text) + 20;
	resize( w, this->fontMetrics().height() + 12 );
	_text = text;
}