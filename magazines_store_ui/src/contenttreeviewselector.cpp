#include "contenttreeviewselector.h"

#include <QFont>
#include <QFontMetrics>
#include <QPainter>
#include <QMouseEvent>
#include <QDebug>

#include "contenttreemodel.h"
#include "basecontentitem.h"
#include "metrics.h"
#include "drawutils.h"

ContentTreeViewSelector::ContentTreeViewSelector(QWidget *parent)
	: QWidget(parent)
{
	_pixmaps[pixmapArrowOpen]	= QPixmap(":/Resources/light_arrow_open.png");
	_pixmaps[pixmapArrowClose]	= QPixmap(":/Resources/light_arrow_close.png");
	_pixmaps[pixmapHideButton]	= QPixmap(":/Resources/selector_hide.png");
	
	_rectHideButton = _pixmaps[pixmapHideButton].rect();
	_textOffset		= 0;
	_counter		= 0;
	_sectionID		= -1;
	
	_fDrawHideButton = false;
}

ContentTreeViewSelector::~ContentTreeViewSelector()
{

}

QSize ContentTreeViewSelector::sizeHint() const
{
	return QSize(-1,-1);
}

void ContentTreeViewSelector::paintEvent( QPaintEvent * )
{
	QPainter p(this);

	QRect rectTotal = rect().adjusted(0,0,-1,0);
	
	DrawUtils::drawSelectedItem( &p, rectTotal, _text, QColor(0xf5,0xf0,0xe1), _font, _textOffset, _counter );

	if(_type == BaseContentItem::tSection)
	{
		ePixmaps idxCurrPixmap = _isExpanded ? pixmapArrowOpen : pixmapArrowClose;

		QRect rectPixmap = rectTotal;
		rectPixmap.setLeft( rectTotal.right() - LeftPanelCounterWidth );

		int offsetX = (rectPixmap.width() - _pixmaps[idxCurrPixmap].width()) / 2;
		int offsetY = (rectPixmap.height() - _pixmaps[idxCurrPixmap].height()) / 2;

		p.drawPixmap( rectPixmap.topLeft() + QPoint(offsetX, offsetY), _pixmaps[idxCurrPixmap] );
	}
	if( _fDrawHideButton )
	{
		p.drawPixmap( _rectHideButton, _pixmaps[pixmapHideButton] );
	}
}

void ContentTreeViewSelector::setupForIndex( const QModelIndex& index, bool isExpanded /*= false*/ )
{
	_sectionID	= index.data(ContentTreeModel::SectionIdRole).toInt();
	_type		= index.data(ContentTreeModel::ContentItemTypeRole).toInt();
	_font		= index.data(Qt::FontRole).value<QFont>();
	_text		= index.data(Qt::DisplayRole).toString();
	_counter	= index.data(ContentTreeModel::CounterRole).toInt();

	_fDrawHideButton = index.data(ContentTreeModel::IsDrawHideButtonRole).toBool();
	
	setAttribute(Qt::WA_TransparentForMouseEvents, !_fDrawHideButton);

	_isExpanded = isExpanded;

	if(index.data(ContentTreeModel::LevelRole).toInt() == 1)
		_textOffset = LeftPanelTextIndent2;
	else
		_textOffset = LeftPanelTextIndent3;
	
	resize( qMax( int(LeftPanelItemWidth + 1), QFontMetrics(_font).width(_text) + 40 ), LeftPanelItemHeight );
	
	if( _fDrawHideButton ) _rectHideButton.moveTopRight( rect().topRight() );
}	

void ContentTreeViewSelector::mousePressEvent( QMouseEvent *e )
{
	if(_rectHideButton.contains(e->pos()))
	{
		hide();
		//////////////////////////////////////////////////////////////////////////
		emit sigHideItem( _sectionID, _text );
		//////////////////////////////////////////////////////////////////////////
		e->accept();
	}
	else
	{
		e->ignore();
	}
}

