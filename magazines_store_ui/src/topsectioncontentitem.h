#ifndef TOPSECTIONCONTENTITEM_H
#define TOPSECTIONCONTENTITEM_H

#include "sectioncontentitem.h"
#include "hidesectioncontentitem.h"

class TopSectionContentItem : public SectionContentItem
{
public:
	TopSectionContentItem( const QString& text, int sectionID, bool isHideEnabled );
	virtual ~TopSectionContentItem();

	//////////////////////////////////////////////////////////////////////////

	//////////////////////////////////////////////////////////////////////////
	virtual int counter( ) const;
	virtual bool isDrawHideButton( ) const;

	virtual BaseContentItem* parent( ) const;

	virtual bool acceptDrops() const;
	virtual int sectionID( ) const;
	
	//////////////////////////////////////////////////////////////////////////
	bool hideChild( int position );
	bool showChild( int position );
	
	BaseContentItem* findSubItem( const QString& name );
	HideSectionContentItem* hideSection() const;

private:
	int							_sectionID;
	QString						_text;

	bool						_isHideEnabled;
	HideSectionContentItem*		_hideSection;
};

#endif // TOPSECTIONCONTENTITEM_H
