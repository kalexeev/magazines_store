#ifndef DRAWUTILS_H
#define DRAWUTILS_H

#include <QPainter>

class DrawUtils
{
public:
	static void addArrowRect( QPainterPath& path, const QRect& rect, qreal xRadius, qreal yRadius );
	static void drawTextWithShadow( QPainter* painter,
									const QString& text, 
									const QPoint& pntBaseLine );
	static void drawTextEx( QPainter* painter, 
							const QRect& rect, 
							const QString& text, 
							int flags /*= Qt::AlignCenter*/ );

	static void drawItem( QPainter* painter, 
		const QRect& rectItem, const QString& text,
		const QColor& clrText, const QFont& font,
		int textIndent, int counter1 = 0, int counter2 = 0 );

	static void drawSelectedItem( QPainter* painter, 
		const QRect& rectItem, const QString& text,
		const QColor& clrText, const QFont& font,
		int textIndent, int counter1 = 0, int counter2 = 0 );

	static void drawSelectionFrame( QPainter* painter, const QRect& rectItem );

private:
	static QFont	_fontCounter;
};

#endif