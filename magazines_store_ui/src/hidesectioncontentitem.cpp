#include "hidesectioncontentitem.h"

#include <QApplication>

HideSectionContentItem::HideSectionContentItem( SectionContentItem* parent  ) 
	: SectionContentItem( QString(), parent )
{
}

HideSectionContentItem::~HideSectionContentItem()
{

}

//////////////////////////////////////////////////////////////////////////

QString HideSectionContentItem::text() const
{
	return QApplication::tr("%0 hidden").arg( SectionContentItem::childsCount() );
}

void HideSectionContentItem::setText( const QString& )
{
	/* EMPTY */
}

bool HideSectionContentItem::isHidden() const
{
	return true;
}

QSize HideSectionContentItem::sizeHint() const
{
	if(childsCount() == 0) 
		return QSize(0, 0);
	return SectionContentItem::sizeHint();
}
