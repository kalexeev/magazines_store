#ifndef SECTIONCONTENTITEM_H
#define SECTIONCONTENTITEM_H

#include "basecontentitem.h"

#include <QList>

class SectionContentItem : public BaseContentItem
{
public:
	SectionContentItem( const QString& text, SectionContentItem* parent );
	virtual ~SectionContentItem( );

	//////////////////////////////////////////////////////////////////////////

	virtual Type type( ) const { return BaseContentItem::tSection; }

	//////////////////////////////////////////////////////////////////////////
	virtual int level( ) const;
	virtual bool isHidden() const;

	virtual bool canDrag() const { return false; }
	virtual bool acceptDrops() const { return true; }

	virtual QString text( ) const;
	virtual void setText( const QString& );

	virtual int counter( ) const { return 0; }
	virtual void setCounter( int counter ) { }

	virtual bool isLocked( ) const { return false; }
	virtual void setLocked( bool fLocked ) { }



	virtual int childsCount( ) const;
	virtual BaseContentItem* child( int position ) const;

	virtual bool insertChild( int position );
	virtual bool removeChild( int position );
	virtual BaseContentItem* takeChild( int position );

	virtual BaseContentItem* parent( ) const;
	virtual void setParent( BaseContentItem* );

	virtual int sectionID( ) const;
	virtual int position( ) const;
	virtual int childPosition( const BaseContentItem*  ) const;

	bool addChild( BaseContentItem* );
protected:
	SectionContentItem*			_parent;
	QString						_text;
	QList< BaseContentItem* >	_childs;
};

#endif // SECTIONCONTENTITEM_H
