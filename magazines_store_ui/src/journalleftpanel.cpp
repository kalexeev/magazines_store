#include "journalleftpanel.h"

#include <QPaintEvent>
#include <QPainter>
#include <QStyleOption>
#include <QLayout>
#include <QStyle>

#include "leftpanelfull.h"
#include "leftpanelhidden.h"

JournalLeftPanel::JournalLeftPanel(QWidget *parent) : QStackedWidget(parent)
{
	memset( &_ui, 0, sizeof(_ui) );
	initUI();

	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
	setContentsMargins(0,0,0,0);
}

JournalLeftPanel::~JournalLeftPanel()
{
}

void JournalLeftPanel::initUI()
{
	_ui.fullPanel		= new LeftPanelFull( this, parentWidget() );
	_ui.hiddenPanel		= new LeftPanelHidden( this );

	connect(_ui.fullPanel, SIGNAL(hideButtonClicked()), this, SLOT(switchToHiddenMode()));
	connect(_ui.hiddenPanel, SIGNAL(showButtonClicked()), this, SLOT(switchToFullMode()));

	addWidget( _ui.fullPanel );
	addWidget( _ui.hiddenPanel );

	setCurrentWidget( _ui.fullPanel );
}

void JournalLeftPanel::switchToFullMode()
{
	if (currentWidget() !=0) {
		currentWidget()->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	}
	setCurrentWidget( _ui.fullPanel );
	currentWidget()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	adjustSize();

	setProperty("hidden", false);
	style()->unpolish(this);
	style()->polish(this);
}

void JournalLeftPanel::switchToHiddenMode()
{
	if (currentWidget() !=0) {
		currentWidget()->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
	}
	setCurrentWidget( _ui.hiddenPanel );
	currentWidget()->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	adjustSize();

	setProperty("hidden", true);
	style()->unpolish(this);
	style()->polish(this);
}

void JournalLeftPanel::mousePressEvent( QMouseEvent *e )
{
	if(property("hidden").toBool() || ((this->rect().right() - e->pos().x()) < 20))
	{
		if(currentWidget() == _ui.fullPanel)
			switchToHiddenMode();
		else
			switchToFullMode();
	}
}
