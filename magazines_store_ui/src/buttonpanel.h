#ifndef BUTTONPANEL_H
#define BUTTONPANEL_H

#include <QWidget>

#include "buttonpaneltooltip.h"

class ButtonPanel : public QWidget
{
	Q_OBJECT

public:
	ButtonPanel(QWidget *parent);
	~ButtonPanel();

protected:
	bool event(QEvent * e);
	
private:
	ButtonPanelTooltip*		_toolTip;
	
};

#endif // BUTTONPANEL_H
