#ifndef CONTENTTREEVIEW_H
#define CONTENTTREEVIEW_H

#include <QTreeView>
#include "contenttreeviewselector.h"
#include "contenttreemodel.h"

class ContentTreeView : public QTreeView
{
	Q_OBJECT

public:
	ContentTreeView(QWidget *parent, QWidget* parentSelector);
	~ContentTreeView();

	virtual void setModel( QAbstractItemModel *model );

protected:
 	virtual void dragEnterEvent( QDragEnterEvent * );
 	virtual void dragMoveEvent( QDragMoveEvent * );
	virtual void dropEvent( QDropEvent * );
	virtual void startDrag( Qt::DropActions supportedActions );

	virtual void drawRow ( QPainter * painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	virtual void drawBranches(QPainter *painter, const QRect &rect, const QModelIndex &index) const;

	virtual void hideEvent( QHideEvent * );
	virtual void showEvent( QShowEvent * );

	virtual void mousePressEvent( QMouseEvent * );
	virtual void timerEvent( QTimerEvent * );
	
	void updateSelector( const QPoint& posCursor );

private:
	int							_selectorTimer;
	int							_dragSection;
	bool						_dragIsHidden;

	QWidget*					_parentSelector;
	ContentTreeViewSelector*	_selector;
};

#endif // CONTENTTREEVIEW_H
