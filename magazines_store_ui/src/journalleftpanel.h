#ifndef JOURNALLEFTPANEL_H
#define JOURNALLEFTPANEL_H

#include <QWidget>
#include <QStackedWidget>

class LeftPanelFull;
class LeftPanelHidden;

class JournalLeftPanel : public QStackedWidget
{
	Q_OBJECT

public:
	JournalLeftPanel(QWidget *parent);
	~JournalLeftPanel();

protected:
	void initUI( );

	void mousePressEvent(QMouseEvent *);
	
public slots:
	void switchToFullMode( );
	void switchToHiddenMode( );

private:
	struct
	{
		LeftPanelFull*		fullPanel;
		LeftPanelHidden*	hiddenPanel;
	} _ui;
};

#endif // JOURNALLEFTPANEL_H
