#ifndef CONTENTTREEMODELSORTPROXY_H
#define CONTENTTREEMODELSORTPROXY_H

#include <QSortFilterProxyModel >

class ContentTreeModelSortProxy : public QSortFilterProxyModel 
{
	Q_OBJECT

public:
	ContentTreeModelSortProxy(QObject *parent);
	~ContentTreeModelSortProxy();
	
	void setSourceModel ( QAbstractItemModel * sourceModel );

	bool lessThan(const QModelIndex &left, const QModelIndex &right) const;
signals:
	void sigHideItem( int sectionID, const QString& itemText );

private:
	
};

#endif // CONTENTTREEMODELSORTPROXY_H
