#ifndef MAINMENULIST_H
#define MAINMENULIST_H

#include <QListWidget>

class MainMenuList : public QListWidget
{
	Q_OBJECT

public:
	enum Items
	{
		iTableOfContents,
		iFavourite,
		iNotes,
		iEvents,
		iDownloads,
		iShopping,
		iSettings,
		iMAX
	};

	MainMenuList(QWidget *parent);
	~MainMenuList();

protected:
	void initMenuItems( );

	bool eventFilter(QObject *, QEvent *);

private:
	QListWidgetItem*	_items[ iMAX ];	
};

#endif // MAINMENULIST_H
