#ifndef JOURNALMAINWIDGET_H
#define JOURNALMAINWIDGET_H

#include <QWidget>
#include <QDeclarativeView>

#include "journalleftpanel.h"

class JournalMainWidget : public QWidget
{
	Q_OBJECT

public:
	JournalMainWidget(QWidget *parent = NULL);
	~JournalMainWidget();

protected:
	void paintEvent(QPaintEvent *);
	void mouseDoubleClickEvent(QMouseEvent *);

private:
	JournalLeftPanel*	_panelLeft;
	QDeclarativeView*	_viewContent;	
};

#endif // JOURNALMAINWIDGET_H
