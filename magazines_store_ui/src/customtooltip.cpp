#include "./customtooltip.h"

#include <QHBoxLayout>
#include <QShowEvent>
#include <QTimerEvent>
#include <QPainter>
#include <QFontMetrics>

#define FADE_IN_DURATION_MS		250
#define FADE_IN_DISCRETE		50

CustomToolTip::CustomToolTip(QWidget *parent)
	: QWidget(parent)
{
	_delayTimer     = -1;
	_fadeInTimer    = -1;
	_currentOpacity = 0;

	_fnt = QFont("Calibri", 11, QFont::Normal);
	setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
	//setWindowFlags(Qt::ToolTip);
}

CustomToolTip::~CustomToolTip()
{
}

void CustomToolTip::setText( const QString& text )
{
	QFontMetrics fm(_fnt);
	
	int w = qMax(175, fm.width(text) + 40);

	resize(w, 25);

	_text = text;
}


void CustomToolTip::timerEvent( QTimerEvent *e )
{
	if(e->timerId() == _fadeInTimer)
	{
		_currentOpacity++;
		setWindowOpacity(((float)_currentOpacity/(float)FADE_IN_DISCRETE));

		if(_currentOpacity == FADE_IN_DISCRETE)
		{
			killTimer(_fadeInTimer);
			_fadeInTimer = -1;
		}
	}
	else if(e->timerId() == _delayTimer)
	{
		killTimer(_delayTimer);
		_delayTimer = -1;

		startFadeIn();
	}
}

void CustomToolTip::hideEvent( QHideEvent * )
{
	setWindowOpacity(1);
	if(_fadeInTimer != -1)
	{
		killTimer(_fadeInTimer);
		_fadeInTimer = -1;
	}
	if(_delayTimer != -1)
	{
		killTimer(_delayTimer);
		_delayTimer = -1;
	}
}

void CustomToolTip::startFadeIn()
{
	int interval = FADE_IN_DURATION_MS/FADE_IN_DISCRETE;
	interval = qMax(1, interval);

	_fadeInTimer = startTimer(interval);
}

void CustomToolTip::showWithDelay(int delay)
{
	_currentOpacity = 0;
	setWindowOpacity(0);
	show();
	_delayTimer = startTimer(delay);
}

void CustomToolTip::paintEvent( QPaintEvent *e )
{
	QPainter p(this);

	QRect rSel = rect().adjusted(1,1,-1,-1);
	
	p.setRenderHint(QPainter::Antialiasing);
	p.setPen(QPen(QColor(77,72,67), 1.25));
	p.setBrush(QColor(128,121,111));
	p.drawRoundedRect(rSel, 10, 10);
	p.setRenderHint(QPainter::Antialiasing,false);

	p.setPen(QPen(QColor(0xf5,0xf0,0xe1), 1));
	p.setFont(_fnt);

	rSel = rSel.adjusted(20,0,0,0);
	p.drawText(rSel, Qt::AlignVCenter|Qt::AlignLeft, _text);
}

QSize CustomToolTip::sizeHint() const
{
	return QSize(200, 23);
}