#ifndef CONTENTTREEDELEGATE_H
#define CONTENTTREEDELEGATE_H

#include <QStyledItemDelegate>

class ContentTreeDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	enum ePixmaps
	{
		pixmapArrowOpen,
		pixmapArrowClose,
		pixmapLock,
		pixmapOrangeCounter,
		pixmapMAX
	};
	ContentTreeDelegate(QObject *parent);
	~ContentTreeDelegate();

	void paint( QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;

protected:
	void drawTopLevelItem( QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	void drawSimpleItem( QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;

private:
	QPixmap		_pixmaps[ pixmapMAX ];
};

#endif // CONTENTTREEDELEGATE_H
