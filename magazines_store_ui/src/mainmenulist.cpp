#include "mainmenulist.h"
#include "mainmenudelegate.h"
#include "metrics.h"

#include <QMouseEvent>
#include <QFont>
#include <QTimeLine>

MainMenuList::MainMenuList(QWidget *parent)
	: QListWidget(parent)
{
	MainMenuDelegate* menuDelegate = new MainMenuDelegate(this);
	setItemDelegate( menuDelegate );

	QTimeLine *timeLine = new QTimeLine(3000, this);
	timeLine->setFrameRange(0, 8);
	connect(timeLine, SIGNAL(frameChanged(int)), menuDelegate, SLOT(setAnimationStep(int)));
	connect(timeLine, SIGNAL(frameChanged(int)), viewport(), SLOT(update()));
	timeLine->setLoopCount(0);
	timeLine->start();


	setViewMode(QListView::ListMode);
	setEditTriggers( QAbstractItemView::NoEditTriggers );

	setGridSize( QSize(LeftPanelItemWidth, LeftPanelItemHeight + 4) );
	setUniformItemSizes(true);
	
	viewport()->setFixedWidth( LeftPanelItemWidth + 5 );

	viewport()->installEventFilter( this );

	setMouseTracking(true);
	setSelectionMode(QAbstractItemView::SingleSelection);

	initMenuItems( );
}

MainMenuList::~MainMenuList()
{
}

bool MainMenuList::eventFilter( QObject *obj, QEvent *e )
{
	if( obj == viewport() )
	{
		switch(e->type())
		{
		case QEvent::Leave:
			{
				selectionModel()->clearSelection();
				update();
			}
			break;
		case QEvent::MouseMove:
			{
				QMouseEvent* me = static_cast<QMouseEvent*>(e);
				selectionModel()->setCurrentIndex(indexAt(me->pos()), QItemSelectionModel::ClearAndSelect);
			}
			break;
		}
	}
	return QListView::eventFilter(obj, e);
}

void MainMenuList::initMenuItems()
{
	_items[ iTableOfContents ] = new QListWidgetItem( tr("Table of contents") );
	_items[ iFavourite ] = new QListWidgetItem( tr("Favourites") );
	_items[ iNotes ] = new QListWidgetItem( tr("Notes") );
	_items[ iEvents ] = new QListWidgetItem( tr("Events") );
	_items[ iDownloads ] = new QListWidgetItem( tr("Downloads") );
	_items[ iShopping ] = new QListWidgetItem( tr("Shopping") );
	_items[ iSettings ] = new QListWidgetItem( tr("Settings") );

	int nDownloads = (qrand()  % 4) + 3;
	_items[ iDownloads ]->setData( MainMenuDelegate::Counter1Role, nDownloads );
	_items[ iDownloads ]->setData( MainMenuDelegate::Counter2Role, nDownloads - 2 );
	_items[ iDownloads ]->setData( MainMenuDelegate::ActivityRole, true );

	QFont fontItem = QFont("Calibri");
	fontItem.setPixelSize( 15 );
	fontItem.setBold( true );

	for( int i = 0; i < iMAX; i++ )
	{
		_items[i]->setData( Qt::ForegroundRole, QColor( 77, 77, 77 ) );
		_items[i]->setData( Qt::FontRole, fontItem );
		addItem( _items[i] );
	}
}
