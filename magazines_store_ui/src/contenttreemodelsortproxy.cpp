#include "contenttreemodelsortproxy.h"

#include "basecontentitem.h"

ContentTreeModelSortProxy::ContentTreeModelSortProxy(QObject *parent)
	: QSortFilterProxyModel (parent)
{

}

ContentTreeModelSortProxy::~ContentTreeModelSortProxy()
{

}

//////////////////////////////////////////////////////////////////////////

bool ContentTreeModelSortProxy::lessThan( const QModelIndex &left, const QModelIndex &right ) const
{
	if( !left.parent().isValid() && !right.parent().isValid() )
	{
		return left.row() < right.row();
	}
	BaseContentItem* itemLeft = static_cast<BaseContentItem*> (left.internalPointer());
	BaseContentItem* itemRight = static_cast<BaseContentItem*> (right.internalPointer());

	if(itemLeft == NULL || itemRight == NULL) 
		return false;

	if(itemLeft->type() == itemRight->type()) 
		return (itemLeft->text()) < (itemRight->text());
	return itemLeft->type() < itemRight->type();
}

void ContentTreeModelSortProxy::setSourceModel( QAbstractItemModel * sourceModel )
{
	connect( this, SIGNAL(sigHideItem(int,const QString&)), sourceModel, SLOT(onHideItem(int,const QString&)) );
	QSortFilterProxyModel::setSourceModel( sourceModel );
}
