#include "leftpanelhidden.h"

#include <QPaintEvent>
#include <QStyleOption>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPainter>

#include <QLabel>
#include <QToolButton>

#include "buttonpanel.h"

LeftPanelHidden::LeftPanelHidden(QWidget *parent)
	: QWidget(parent)
{
	memset( &_ui, 0, sizeof(_ui) );
	initUI();
}

LeftPanelHidden::~LeftPanelHidden()
{

}

void LeftPanelHidden::initUI()
{
	_ui.buttonsMenu = new ButtonPanel(this);

	_ui.btnShow = new QToolButton(this);
	QPixmap pmapBtn(":/Resources/btn_left_show.png");
	_ui.btnShow->setIconSize(pmapBtn.size());
	_ui.btnShow->setIcon( pmapBtn );
	_ui.btnShow->setCursor(Qt::PointingHandCursor);
	_ui.btnShow->setStyleSheet("background-color: transparent; border: 0px solid transparent;");
	connect(_ui.btnShow, SIGNAL(clicked()), this, SIGNAL(showButtonClicked()));

	QHBoxLayout* layoutBottom = new QHBoxLayout;
	layoutBottom->addStretch();
	layoutBottom->addWidget(_ui.btnShow);
	layoutBottom->addStretch();
	layoutBottom->setSpacing(0);
	layoutBottom->setMargin(0);
	
	QVBoxLayout* layoutMain = new QVBoxLayout;
	layoutMain->addStretch();
	layoutMain->addWidget(_ui.buttonsMenu);
	layoutMain->addSpacing(15);
	layoutMain->addLayout(layoutBottom);
	layoutMain->setMargin(0);
	layoutMain->setContentsMargins(5,5,5,5);

	setLayout(layoutMain);
}

void LeftPanelHidden::paintEvent( QPaintEvent * )
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive (QStyle::PE_Widget, &opt, &p, this);
}