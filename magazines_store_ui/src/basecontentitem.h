#ifndef BASECONTENTITEM_H
#define BASECONTENTITEM_H

#include <QString>
#include <QSize>

#include "metrics.h"

class BaseContentItem
{
public:
	enum Type
	{
		tSimpleItem = 0,
		tSection,
		tSpacer
	};

	BaseContentItem( )			{  }
	virtual ~BaseContentItem( ) { /* EMPTY */ }

	virtual Type type( ) const = 0;

	//////////////////////////////////////////////////////////////////////////
	virtual int level( ) const = 0;
	
	virtual int counter( ) const = 0;
	virtual void setCounter( int value ) = 0;

	virtual bool isLocked( ) const = 0;
	virtual void setLocked( bool fLocked ) = 0;
	
	virtual bool isHidden() const = 0;
	virtual QSize sizeHint() const { return QSize( LeftPanelItemWidth, LeftPanelItemHeight ); }

	virtual bool acceptDrops() const = 0;
	virtual bool canDrag() const = 0;

	virtual QString text( ) const = 0;
	virtual void setText( const QString& ) = 0;

	virtual bool isDrawHideButton( ) const { return false; }

	virtual int childsCount( ) const = 0;
	virtual BaseContentItem* child( int position ) const = 0;
	virtual int childPosition( const BaseContentItem* ) const = 0;

	virtual bool insertChild( int position ) = 0;
	virtual bool removeChild( int position ) = 0;
	virtual BaseContentItem* takeChild( int position ) = 0;
	
	virtual BaseContentItem* parent( ) const = 0; 
	virtual void setParent( BaseContentItem* ) = 0;

	virtual int sectionID( ) const = 0;
	virtual int position( ) const = 0;

private:

};

#endif // BASECONTENTITEM_H
