#include "contenttreedelegate.h"

#include "contenttreemodel.h"
#include "basecontentitem.h"
#include "drawutils.h"

#include <QPainter>

ContentTreeDelegate::ContentTreeDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{
	_pixmaps[pixmapArrowOpen]		= QPixmap(":/Resources/arrow_open.png");
	_pixmaps[pixmapArrowClose]		= QPixmap(":/Resources/arrow_close.png");
	_pixmaps[pixmapLock]			= QPixmap(":/Resources/lock.png");
	_pixmaps[pixmapOrangeCounter]	= QPixmap(":/Resources/orange_counter.png");
}

ContentTreeDelegate::~ContentTreeDelegate()
{

}

//////////////////////////////////////////////////////////////////////////

void ContentTreeDelegate::paint( QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	if( index.data(ContentTreeModel::ContentItemTypeRole).toInt() == BaseContentItem::tSpacer) 
		return;
	int level = index.data(ContentTreeModel::LevelRole).toInt();

	painter->save();
	painter->setClipRect( option.rect );
	if(level == 0)
		drawTopLevelItem( painter, option, index );
	else
		drawSimpleItem( painter, option, index );
	painter->restore();
}

void ContentTreeDelegate::drawTopLevelItem( QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	QRect rectItem = option.rect;
	rectItem.setWidth( LeftPanelItemWidth );

	DrawUtils::drawItem( painter, rectItem, 
		index.data(Qt::DisplayRole).toString(),
		index.data(Qt::TextColorRole).value<QColor>(),
		index.data(Qt::FontRole).value<QFont>(),
		LeftPanelTextIndent1,
		index.data(ContentTreeModel::CounterRole).toInt() );

	ePixmaps idxCurrPixmap = (option.state & QStyle::State_Open) ? pixmapArrowOpen : pixmapArrowClose;
	painter->drawPixmap(0, rectItem.top() + (rectItem.height() - _pixmaps[idxCurrPixmap].height()) / 2, _pixmaps[idxCurrPixmap]);
}

void ContentTreeDelegate::drawSimpleItem( QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
// 	if(option.state & QStyle::State_MouseOver && (index.flags() & Qt::ItemIsSelectable)) 
//		return;
	QRect rectItem = option.rect;
	rectItem.setWidth( LeftPanelItemWidth );
	
	DrawUtils::drawItem( painter, rectItem, 
		index.data(Qt::DisplayRole).toString(),
		index.data(Qt::TextColorRole).value<QColor>(),
		index.data(Qt::FontRole).value<QFont>(),
		(index.data(ContentTreeModel::LevelRole).toInt() == 1) ? LeftPanelTextIndent2 : LeftPanelTextIndent3,
		index.data(ContentTreeModel::CounterRole).toInt() );
	
	if( index.data(ContentTreeModel::IsLockedRole).toBool() )
	{
		QRect rectPixmap = rectItem;
		rectPixmap.setLeft( rectItem.right() - LeftPanelCounterWidth );

		int offsetX = (rectPixmap.width() - _pixmaps[pixmapLock].width()) / 2;
		int offsetY = (rectPixmap.height() - _pixmaps[pixmapLock].height()) / 2;

		painter->drawPixmap( rectPixmap.topLeft() + QPoint(offsetX, offsetY), _pixmaps[pixmapLock] );
	}
}

