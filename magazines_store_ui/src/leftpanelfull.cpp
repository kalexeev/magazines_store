﻿#include "leftpanelfull.h"

#include <QPaintEvent>
#include <QStyleOption>
#include <QSizePolicy>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QStringList>
#include <QStringListModel>
#include <QStyledItemDelegate>

#include <QLabel>
#include <QToolButton>

#include "drawutils.h"
#include "contenttreemodel.h"
#include "contenttreemodelsortproxy.h"
#include "contenttreeview.h"
#include "mainmenulist.h"

LeftPanelFull::LeftPanelFull(QWidget *parent, QWidget *parentForSelector)
	: QWidget(parent)
{
	_parentForSelector = parentForSelector;
	
	memset( &_ui, 0, sizeof(_ui) );
	initUI();
}

LeftPanelFull::~LeftPanelFull()
{

}

void LeftPanelFull::initUI()
{
	_ui.contentList = new MainMenuList(this);

	 
	//////////////////////////////////////////////////////////////////////////
	_ui.contentTree = new ContentTreeView(this, _parentForSelector);

	ContentTreeModel* contentTreeModel = new ContentTreeModel(_ui.contentTree);
	contentTreeModel->addSectionItem( ContentTreeModel::SectionAll, tr("SubSection1_1"), 3 );
	contentTreeModel->addSectionItem( ContentTreeModel::SectionAll, tr("SubSection1_2") );
	contentTreeModel->addSectionItem( ContentTreeModel::SectionAll, tr("SubSection1_3"), 4 );
	contentTreeModel->addSectionItem( ContentTreeModel::SectionAll, tr("SubSection1_4"), 0, true );

	contentTreeModel->addSectionItem( ContentTreeModel::SectionPublish, tr("SubSection2_1") );
	contentTreeModel->addSectionItem( ContentTreeModel::SectionPublish, tr("SubSection2_2") );
	contentTreeModel->addSectionItem( ContentTreeModel::SectionPublish, tr("SubSection2_3") );

	contentTreeModel->addSectionItem( ContentTreeModel::SectionRecent, tr("SubSection3_1") );
	contentTreeModel->addSectionItem( ContentTreeModel::SectionRecent, tr("SubSection3_2") );
	contentTreeModel->addSectionItem( ContentTreeModel::SectionRecent, tr("SubSection3_3") );
	contentTreeModel->addSectionItem( ContentTreeModel::SectionRecent, tr("SubSection3_4") );
	contentTreeModel->addSectionItem( ContentTreeModel::SectionRecent, tr("SubSection3_5") );

	contentTreeModel->setItemVisibility( ContentTreeModel::SectionAll, tr("SubSection1_2"), false );

	ContentTreeModelSortProxy* sortProxyModel = new ContentTreeModelSortProxy(this);
	sortProxyModel->setSourceModel(contentTreeModel);
	sortProxyModel->setDynamicSortFilter(true);
	_ui.contentTree->setSortingEnabled(true);
	_ui.contentTree->sortByColumn(0, Qt::AscendingOrder);
	_ui.contentTree->setModel( sortProxyModel );
	//////////////////////////////////////////////////////////////////////////
	_ui.contentTree->setFixedWidth(203);
	_ui.contentList->setFixedWidth(203);

	_ui.lblDelimiter = new QLabel(this);
	_ui.lblDelimiter->setPixmap( QPixmap(":/Resources/delimiter.png") );

	_ui.lblLogo = new QLabel(this);
	_ui.lblLogo->setPixmap( QPixmap(":/Resources/left_logo.png") );

	_ui.btnHide = new QToolButton(this);

	QPixmap pmapBtn(":/Resources/btn_left_hide.png");
	_ui.btnHide->setIconSize(pmapBtn.size());
	_ui.btnHide->setIcon( pmapBtn );
	_ui.btnHide->setCursor(Qt::PointingHandCursor);
	_ui.btnHide->setStyleSheet("background-color: transparent; border: 0px solid transparent;");
	connect(_ui.btnHide, SIGNAL(clicked()), this, SIGNAL(hideButtonClicked()));

	QHBoxLayout* layoutBottom = new QHBoxLayout;
	layoutBottom->addStretch();
	layoutBottom->addWidget(_ui.lblLogo);
	layoutBottom->addStretch();
	layoutBottom->addWidget(_ui.btnHide);

	QVBoxLayout* layoutMain = new QVBoxLayout;
	layoutMain->setSpacing(0);
	layoutMain->addWidget(_ui.contentTree, 2);
	layoutMain->addSpacing(14);
	layoutMain->addWidget(_ui.lblDelimiter);
	layoutMain->addSpacing(18);
	layoutMain->addWidget(_ui.contentList);
	layoutMain->addLayout(layoutBottom);
	layoutMain->setContentsMargins(0,5,8,5);
	
	setLayout(layoutMain);
}

void LeftPanelFull::paintEvent( QPaintEvent * )
{
	QStyleOption opt;
	opt.init(this);
	QPainter p(this);
	style()->drawPrimitive (QStyle::PE_Widget, &opt, &p, this);
}