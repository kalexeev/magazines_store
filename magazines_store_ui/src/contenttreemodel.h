#ifndef CONTENTTREEMODEL_H
#define CONTENTTREEMODEL_H

#include <QAbstractItemModel>
#include <QFont>
#include <QColor>

class TopSectionContentItem;
class BaseContentItem;

class ContentTreeModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	enum eRootSectionsID
	{
		SectionAll	=	0,
		SectionPublish,
		SectionRecent,
		SectionMAX
	};

	enum eModelRoles
	{
		SectionIdRole = Qt::UserRole,
		ContentItemTypeRole,
		LevelRole,
		CounterRole,
		IsHiddenRole,
		IsLockedRole,
		IsDrawHideButtonRole,
		RoleMAX
	};

	enum eFonts
	{
		fontTopItem,
		fontSubItem,
		fontMAX
	};

	enum eTextColor
	{
		textClrVisibleItem,
		textClrHiddenItem,
		textClrMAX
	};

	ContentTreeModel(QObject *parent = 0);
	~ContentTreeModel();

	QVariant data(const QModelIndex &index, int role) const;
	Qt::ItemFlags flags(const QModelIndex &index) const;
	QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const;
	QModelIndex parent(const QModelIndex &index) const;
	int rowCount(const QModelIndex &parent = QModelIndex()) const;
	int columnCount(const QModelIndex &parent = QModelIndex()) const;
	
	bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex());
	bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex());
	bool setData(const QModelIndex &index, const QVariant &value, int role);
	QMap<int, QVariant> itemData(const QModelIndex &index) const;
	
	bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent);
	Qt::DropActions supportedDropActions() const;
 	QMimeData * mimeData ( const QModelIndexList & indexes ) const;

	BaseContentItem* itemFromIndex( const QModelIndex& index ) const;

	void addSectionItem( eRootSectionsID section, const QString& strItem, int counter = 0, bool isLocked = false );
	bool setItemVisibility( eRootSectionsID section, const QString& strItem, bool isVisible );

public slots:
	void onHideItem( int sectionId, const QString& itemText );

protected:
	void initRootSection( );

private:	
	TopSectionContentItem*		_sections[SectionMAX];
	QList< BaseContentItem * >	_rootItems;
	QFont						_fonts[fontMAX];
	QColor						_textColors[textClrMAX];
};


#endif // CONTENTTREEMODEL_H
