#include "buttonpanel.h"

#include <QVBoxLayout>
#include <QToolButton>
#include <QHelpEvent>

ButtonPanel::ButtonPanel(QWidget *parent )
	: QWidget(parent)
{
	_toolTip = new ButtonPanelTooltip( window() );
	_toolTip->hide();

	QVBoxLayout* layoutBtns = new QVBoxLayout;
	
	setStyleSheet("background-color: transparent; border: 0px solid transparent;");
	for(int i = 0; i < 7; i++)
	{
		QPixmap pmapBtn( QString(":/Resources/btn%0.png").arg(i + 1) );
		QToolButton* btn = new QToolButton(this);
		btn->setIconSize( pmapBtn.size() );
 		btn->setIcon( pmapBtn );
 		btn->setCursor(Qt::PointingHandCursor);
 		btn->setStyleSheet("background-color: transparent; border: 0px solid transparent;");
		btn->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
		btn->setProperty("tooltip", tr("Tooltip %0").arg(i+1) );
		layoutBtns->addWidget(btn);
	}

	layoutBtns->setSpacing(3);
	layoutBtns->setMargin(0);
	setLayout(layoutBtns);

	setMouseTracking(true);
}

ButtonPanel::~ButtonPanel()
{
	delete _toolTip;
}

bool ButtonPanel::event(QEvent * e)
{
	if(e->type() == QEvent::ToolTip)
	{
		QHelpEvent* he = static_cast<QHelpEvent *> (e);
		
		QWidget* child = childAt(he->pos());
		if(child)
		{
			QString strText = child->property("tooltip").toString();

			if(!strText.isEmpty())
			{
				QRect rChild = child->geometry();

				_toolTip->setText(strText);
				_toolTip->move( mapTo(window(), QPoint(child->geometry().right(), 
					child->geometry().center().y() - _toolTip->height()/2) ) );
				_toolTip->show();
			}
			else
			{
				_toolTip->hide();
			}			
		}
	} 
	else if(e->type() == QEvent::Leave)
	{
		_toolTip->hide();
	}
	return QWidget::event(e);
}
