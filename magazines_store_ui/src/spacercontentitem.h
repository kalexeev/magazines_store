#pragma once

#include "basecontentitem.h"

class SpacerContentItem : public BaseContentItem
{
public:
	SpacerContentItem( int height ) : BaseContentItem()
	{
		_height = height;
	}
	~SpacerContentItem(void) {  }

	//////////////////////////////////////////////////////////////////////////

	virtual Type type( ) const { return tSpacer; }

	//////////////////////////////////////////////////////////////////////////
	virtual int level( ) const { return 0; }
	virtual bool isHidden() const { return false; }
	virtual QSize sizeHint() const { return QSize( LeftPanelItemWidth, _height); }

	virtual bool acceptDrops() const { return false; }
	virtual bool canDrag() const { return false; }

	virtual QString text( ) const { return QString(); }
	virtual void setText( const QString& ) { }

	virtual int counter( ) const { return 0; }
	virtual void setCounter( int counter ) { }

	virtual bool isLocked( ) const { return false; }
	virtual void setLocked( bool fLocked ) { }

	virtual int childsCount( ) const { return 0; }
	virtual BaseContentItem* child( int position ) const { return NULL; }
	virtual int childPosition( const BaseContentItem* ) const { return 0; }

	virtual bool insertChild( int position ) { return false; }
	virtual bool removeChild( int position ) { return false; }
	virtual BaseContentItem* takeChild( int position ) { return NULL; }

	virtual BaseContentItem* parent( ) const { return NULL; }
	virtual void setParent( BaseContentItem* ) {  }

	virtual int sectionID( ) const { return -1; }
	virtual int position( ) const { return 0; }

private:
	int			_height;
};

