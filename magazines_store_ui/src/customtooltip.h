#ifndef CUSTOMTOOLTIP_H
#define CUSTOMTOOLTIP_H

#include <QWidget>
#include <QString>
#include <QFont>

class CustomToolTip : public QWidget
{
	Q_OBJECT

public:
	CustomToolTip(QWidget *parent);
	~CustomToolTip();

	void setText( const QString& text );
	QString getText( ) { return _text; }

	void showWithDelay( int delay );
	
	QSize sizeHint() const;

protected:
	void paintEvent(QPaintEvent *);

protected:
	void startFadeIn( );
	void hideEvent(QHideEvent *);
	void timerEvent(QTimerEvent *);

private:
	int		_delayTimer;
	int		_fadeInTimer;
	int		_currentOpacity;
	QString _text;
	QFont	_fnt;
};

#endif // CUSTOMTOOLTIP_H
