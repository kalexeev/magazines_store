#ifndef SIMPLECONTENTITEM_H
#define SIMPLECONTENTITEM_H

#include "basecontentitem.h"

class SimpleContentItem : public BaseContentItem
{
public:
	SimpleContentItem( const QString& text, BaseContentItem* parent );
	virtual ~SimpleContentItem( );

	//////////////////////////////////////////////////////////////////////////

	virtual Type type( ) const { return BaseContentItem::tSimpleItem; }

	//////////////////////////////////////////////////////////////////////////
	virtual int level( ) const;
	virtual bool isHidden() const;
	
	virtual bool isDrawHideButton( ) const;

	virtual bool canDrag() const;
	virtual bool acceptDrops() const;

	virtual QString text( ) const;
	virtual void setText( const QString& );

	virtual int counter( ) const;
	virtual void setCounter( int counter );

	virtual bool isLocked( ) const;
	virtual void setLocked( bool fLocked );

	virtual int childsCount( ) const;
	virtual BaseContentItem* child( int position ) const;

	virtual bool insertChild( int position );
	virtual bool removeChild( int position );
	virtual BaseContentItem* takeChild( int position ) { return NULL; }

	virtual BaseContentItem* parent( ) const;
	virtual void setParent( BaseContentItem* );
	
	virtual int sectionID( ) const;
	virtual int position( ) const;
	virtual int childPosition( const BaseContentItem* ) const;
	
private:
	BaseContentItem*		_parent;
	QString					_text;
	int						_counter;
	bool					_isLocked;
};

#endif // SIMPLECONTENTITEM_H
