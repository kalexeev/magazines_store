#include "topsectioncontentitem.h"

TopSectionContentItem::TopSectionContentItem( const QString& text, int sectionID, bool isHideEnabled )
	: SectionContentItem( text, NULL )
{
	_sectionID		= sectionID;
	_text			= text;
	
	_isHideEnabled	= isHideEnabled;
	if(_isHideEnabled)
	{
		_hideSection = new HideSectionContentItem( this );
		_childs.prepend( _hideSection );
	}
	else
	{
		_hideSection = NULL;
	}
}

TopSectionContentItem::~TopSectionContentItem()
{
}

//////////////////////////////////////////////////////////////////////////

BaseContentItem* TopSectionContentItem::parent() const
{
	return NULL;
}

int TopSectionContentItem::sectionID() const
{
	return _sectionID;
}

int TopSectionContentItem::counter() const
{
	int result = 0; 

	for(int i = 0; i < childsCount(); i++)
	{
		result += child(i)->counter();
	}
	return result;
}

bool TopSectionContentItem::hideChild( int position )
{
	if( !_isHideEnabled ) return false;

	if( position < childsCount() )
	{
		BaseContentItem* item = takeChild(position);
		if(item && (item->type() == BaseContentItem::tSimpleItem))
		{
			return _hideSection->addChild( item );
		}
		else
		{
			Q_ASSERT(false);
		}
	}
	return false;
}

bool TopSectionContentItem::showChild( int position )
{
	if( !_isHideEnabled ) return false;

	if( position < _hideSection->childsCount() )
	{
		BaseContentItem* item = _hideSection->takeChild(position);
		if(item && (item->type() == BaseContentItem::tSimpleItem))
		{
			return addChild(item);
		}
		else
		{
			Q_ASSERT(false);
		}
	}
	return false;
}

BaseContentItem* TopSectionContentItem::findSubItem( const QString& name )
{
	for(int i = 0; i < _childs.size(); i++)
	{
		if(_childs.at(i)->text() == name)
			return _childs.at(i);
	}
	if(_isHideEnabled)
	{
		for(int i = 0; i < _hideSection->childsCount(); i++)
		{
			if(_hideSection->child(i)->text() == name)
				return _hideSection->child(i);
		}
	}
	return NULL;
}

HideSectionContentItem* TopSectionContentItem::hideSection() const
{
	return _hideSection;
}

bool TopSectionContentItem::acceptDrops() const
{
	if( _isHideEnabled )
		return _hideSection->childsCount() > 0;
	return false;
}

bool TopSectionContentItem::isDrawHideButton() const
{
	if(_isHideEnabled)
		return (_hideSection->childsCount() == 0);
	return false;
}
