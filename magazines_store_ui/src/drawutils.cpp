#include "drawutils.h"
#include "metrics.h"

QFont DrawUtils::_fontCounter = QFont("Helvetica", 8, QFont::Normal);

void DrawUtils::addArrowRect( QPainterPath& path, const QRect& rect, qreal xRadius, qreal yRadius )
{
	QRectF r = rect.normalized();

	if (r.isNull())
		return;

	QRectF rectForRect = r.adjusted(r.height()/2,0,0,0);

	qreal wForArc = rectForRect.width() / 2;
	qreal hForArc = rectForRect.height() / 2;

	if (wForArc == 0) {
		xRadius = 0;
	} else {
		xRadius = 100 * qMin(xRadius, wForArc) / wForArc;
	}
	if (hForArc == 0) {
		yRadius = 0;
	} else {
		yRadius = 100 * qMin(yRadius, hForArc) / hForArc;
	}

	qreal x = rectForRect.x();
	qreal y = rectForRect.y();
	qreal w = rectForRect.width();
	qreal h = rectForRect.height();
	qreal rxx2 = w*xRadius/100;
	qreal ryy2 = h*yRadius/100;

	path.moveTo(x, y);
	path.arcTo(x+w-rxx2, y, rxx2, ryy2, 90, -90);
	path.arcTo(x+w-rxx2, y+h-ryy2, rxx2, ryy2, 0, -90);
	path.lineTo(x, y+h);
	path.lineTo(r.left(), r.top() + r.height()/2);
	path.lineTo(x, y);
	path.closeSubpath();
}

void DrawUtils::drawTextWithShadow( QPainter* painter, const QString& text, const QPoint& pntBaseLine )
{
	QPoint pntToDraw = pntBaseLine;
	QPen penBefore = painter->pen();
	painter->setPen(Qt::white);
	painter->drawText(pntToDraw + QPoint(0,1), text);

	painter->setPen(penBefore);
	painter->drawText(pntToDraw, text);
}

void DrawUtils::drawTextEx( QPainter* painter, 
							const QRect& rect, 
							const QString& text, 
							int flags /*= Qt::AlignCenter*/ ) 
{
	QFontMetrics fm = painter->fontMetrics();
	int x = 0, y = 0;

	if(flags & Qt::AlignHCenter || ((flags & Qt::AlignHorizontal_Mask) == 0)) {
		x = rect.x() + (rect.width() - fm.width(text) + 1) / 2;
	} 
	else if(flags & Qt::AlignLeft) {
		x = rect.x();
	}
	else if(flags & Qt::AlignRight) {
		x = rect.right() - fm.width(text);
	}

	if(flags & Qt::AlignVCenter || ((flags & Qt::AlignVertical_Mask) == 0)) {
		y = rect.y() + (rect.height() - (rect.height() - fm.height()) / 2 - fm.descent() - 1);
	}
	else if(flags & Qt::AlignTop) {
		y = rect.y() + (fm.height() - fm.descent() - 1);
	}
	else if(flags & Qt::AlignBottom) {
		y = rect.bottom() - fm.descent() - 1;
	}

	drawTextWithShadow ( painter, text, QPoint(x, y) );
}

void DrawUtils::drawItem( QPainter* painter, const QRect& rectItem, const QString& text, const QColor& clrText, const QFont& font, int textIndent, int counter1 /*= 0*/, int counter2 /*= 0*/ )
{
	QRect rect = rectItem.adjusted( LeftPanelItemPaddingLeft, 1, 0, -1 );
	QRect rectText = rect.adjusted( textIndent, 0, 0, 0 );

	if( counter1 > 0 )
	{
		QRect rectCounter = rect;
		rectCounter.setLeft( rectCounter.right() - LeftPanelCounterWidth );
		rectText.setRight( rectCounter.left() );

		painter->setRenderHint( QPainter::Antialiasing );
		painter->setPen( QPen(QColor(181,86,45), 1) );
		painter->setBrush( QColor(232,110,57) );
		painter->drawRoundedRect( rectCounter, 10, 10 );
		painter->setRenderHint( QPainter::Antialiasing,false );
		painter->setPen( Qt::white );

		painter->setFont( _fontCounter );
		painter->drawText( rectCounter, Qt::AlignCenter, QString::number(counter1) );
	}
	if( counter2 > 0 )
	{
		QRect rectCounter = rect;
		rectCounter.setLeft( rectCounter.right() - LeftPanelCounterWidth*2 - 3 );
		rectCounter.setRight( rectCounter.right() - LeftPanelCounterWidth - 3 );
		rectText.setRight( rectCounter.left() );

		painter->setRenderHint( QPainter::Antialiasing );
		painter->setPen( QPen(QColor(88,112,49), 1) );
		painter->setBrush( QColor(124,157,68) );
		painter->drawRoundedRect( rectCounter, 10, 10 );
		painter->setRenderHint( QPainter::Antialiasing,false );
		painter->setPen( Qt::white );

		painter->setFont( _fontCounter );
		painter->drawText( rectCounter, Qt::AlignCenter, QString::number(counter2) );
	}

	QFontMetrics fm = QFontMetrics(font);
	QString textToDraw = text;
	if(fm.width( textToDraw ) > rectText.width())
		textToDraw = fm.elidedText( text, Qt::ElideRight, rectText.width() );
	
	painter->setFont( font );
	painter->setPen( clrText );
	DrawUtils::drawTextEx( painter, rectText, textToDraw, Qt::AlignVCenter | Qt::AlignLeft );
}

void DrawUtils::drawSelectedItem( QPainter* painter, const QRect& rectItem, const QString& text, const QColor& clrText, const QFont& font, int textIndent, int counter1 /*= 0*/, int counter2 /*= 0 */ )
{
	QRect rect = rectItem.adjusted( LeftPanelItemPaddingLeft, 0, 0, 0 );
	QRect rectText = rect.adjusted( textIndent, 0, 0, 0 );
	QRect rectSelection = rect;

	painter->setRenderHint( QPainter::Antialiasing );
	painter->setPen( QPen(QColor(77,72,67), 1.0) );
	painter->setBrush( QColor(128,121,111));
	painter->drawRoundedRect( rectSelection, 10, 10 );
	painter->setRenderHint( QPainter::Antialiasing,false );

	if( counter1 > 0 )
	{
		QRect rectCounter = rect;
		rectCounter.setLeft( rectCounter.right() - LeftPanelCounterWidth );
		rectText.setRight( rectCounter.left() );
			
		painter->setFont( _fontCounter );
		painter->setPen( QPen(QColor(0xf5,0xf0,0xe1), 1) );
		painter->drawText( rectCounter, Qt::AlignCenter, QString::number(counter1) );
	}
	if( counter2 > 0 )
	{
		QRect rectCounter = rect;
		rectCounter.setLeft( rectCounter.right() - LeftPanelCounterWidth*2 - 3 );
		rectCounter.setRight( rectCounter.right() - LeftPanelCounterWidth - 3 );
		rectText.setRight( rectCounter.left() );

		painter->setFont( _fontCounter );
		painter->setPen( QPen(QColor(0xf5,0xf0,0xe1), 1) );
		painter->drawText( rectCounter, Qt::AlignCenter, QString::number(counter2) );
	}

	painter->setPen( QPen(QColor(0xf5,0xf0,0xe1), 1) );
	painter->setFont( font );
	painter->drawText( rectText, Qt::AlignVCenter | Qt::AlignLeft, text );
}

void DrawUtils::drawSelectionFrame( QPainter* painter, const QRect& rectItem )
{
	QRect rectSelection = rectItem.adjusted( LeftPanelItemPaddingLeft, 0, 0, 0 );

	painter->setPen( QPen(QColor(77,72,67), 1) );
	painter->setBrush( Qt::NoBrush );
	painter->setRenderHint( QPainter::Antialiasing );
	painter->drawRoundedRect( rectSelection, 10, 10 );
	painter->setRenderHint( QPainter::Antialiasing, false );
}
