#ifndef BUTTONPANELTOOLTIP_H
#define BUTTONPANELTOOLTIP_H

#include <QWidget>

class ButtonPanelTooltip : public QWidget
{
	Q_OBJECT

public:
	ButtonPanelTooltip(QWidget *parent);
	~ButtonPanelTooltip();

	void setText( const QString& text );
	QString text( ) const { return _text; }

protected:
	void paintEvent(QPaintEvent *);

private:
	QString		_text;
};

#endif // BUTTONPANELTOOLTIP_H
