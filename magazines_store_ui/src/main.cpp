#include "journalmainwidget.h"

#include <QtWidgets/QApplication>
#include <QFile>
#include <QCommonStyle>
#include <QTranslator>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	qApp->setStyle( new QCommonStyle );

	QFile file(":/Resources/style.qss");
	file.open(QFile::ReadOnly);
	QString styleSheet = QLatin1String(file.readAll());
	qApp->setStyleSheet(styleSheet);
	
	QTranslator tr;
	tr.load(":/Resources/magazines_store_ui_ru.qm");
	qApp->installTranslator( &tr );

	JournalMainWidget w;
	w.show();

	return a.exec();
}
