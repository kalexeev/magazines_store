#ifndef CONTENTTREEVIEWSELECTOR_H
#define CONTENTTREEVIEWSELECTOR_H

#include <QWidget>
#include <QModelIndex>

class ContentTreeViewSelector : public QWidget
{
	Q_OBJECT

	enum ePixmaps
	{
		pixmapArrowOpen,
		pixmapArrowClose,
		pixmapHideButton,
		pixmapMAX
	};

public:
	ContentTreeViewSelector(QWidget *parent);
	~ContentTreeViewSelector();

	void setupForIndex( const QModelIndex& index, bool isExpanded = false );

	QSize sizeHint() const;

signals:
	void sigHideItem( int sectionID, const QString& itemText );

protected:
	void mousePressEvent( QMouseEvent* );
	void paintEvent( QPaintEvent * );
	
private:
	QPixmap				_pixmaps[pixmapMAX];

	bool				_fDrawHideButton;
	QRect				_rectHideButton;
	
	QFont				_font;
	
	int					_sectionID;
	QString				_text;
	int					_textOffset;
	int					_counter;
	bool				_isExpanded;
	int					_type;
};

#endif // CONTENTTREEVIEWSELECTOR_H
