#include "journalmainwidget.h"

#include <QHBoxLayout>
#include <QStyleOption>
#include <QSizePolicy>
#include <QPainter>
#include <QLabel>
#include <QDebug>

JournalMainWidget::JournalMainWidget(QWidget *parent)
	: QWidget(parent)
{
	QHBoxLayout* hLayout = new QHBoxLayout;

	setWindowTitle("Magazines store");
// 	QLabel* lblContents = new QLabel(this);
// 	lblContents->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
// 	lblContents->setAlignment(Qt::AlignCenter);
// 	lblContents->setText("Here will be content");
// 	lblContents->setStyleSheet(
// 		"color: rgba(255,255,255,128); border: 3px solid rgba(255,255,255,128);"
// 		"background: transparent; font: bold 18pt \"Calibri\";" );
	_panelLeft = new JournalLeftPanel(this);
	_viewContent = new QDeclarativeView(this);
	_viewContent->setSource(QUrl::fromLocalFile("./qml/main.qml"));
	_viewContent->setStyleSheet(QString("background: transparent"));
	_viewContent->setResizeMode(QDeclarativeView::SizeRootObjectToView);
	hLayout->addWidget(_panelLeft);
	hLayout->addWidget(_viewContent);

	setLayout(hLayout);
	hLayout->setMargin(0);
	hLayout->setSpacing(4);

	setMinimumSize( QSize(800, 600) );
}

JournalMainWidget::~JournalMainWidget()
{

}

void JournalMainWidget::paintEvent( QPaintEvent * )
{
	QStyleOption opt;
	opt.init (this);
	QPainter p (this);
	style ()->drawPrimitive (QStyle::PE_Widget, &opt, &p, this);
}

void JournalMainWidget::mouseDoubleClickEvent( QMouseEvent * )
{
	qDebug() << "MainWidget DOUBLE CLICKED";
}