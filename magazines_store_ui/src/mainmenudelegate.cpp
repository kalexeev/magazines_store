#include "mainmenudelegate.h"

#include "drawutils.h"
#include "metrics.h"

#include <QPainter>

MainMenuDelegate::MainMenuDelegate(QObject *parent)
	: QStyledItemDelegate(parent)
{
	_pixmapActivity	= QPixmap(":/Resources/activity_indicator.png");
	_animationStep	= 0;
}

MainMenuDelegate::~MainMenuDelegate()
{

}

void MainMenuDelegate::paint( QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	QString title = index.data(Qt::DisplayRole).toString();
	QRect rectItem = option.rect;
	
	rectItem.setWidth( LeftPanelItemWidth );
	
	painter->save();
	painter->setClipRect( option.rect );
	if(option.state & QStyle::State_Selected)
	{
		DrawUtils::drawSelectedItem( painter, rectItem, title, 	
			index.data(Qt::TextColorRole).value<QColor>(),
			index.data(Qt::FontRole).value<QFont>(),
			LeftPanelTextIndent1, 
			index.data(Counter1Role).toInt(),
			index.data(Counter2Role).toInt() );

	}
	else
	{
		DrawUtils::drawItem( painter, rectItem, title, 	
			index.data(Qt::TextColorRole).value<QColor>(),
			index.data(Qt::FontRole).value<QFont>(),
			LeftPanelTextIndent1, 
			index.data(Counter1Role).toInt(),
			index.data(Counter2Role).toInt() );

		if(index.data(ActivityRole).toBool())
		{
			painter->translate( rectItem.left() + _pixmapActivity.width() / 2 + 2, rectItem.center().y() );
			painter->rotate( 45. * _animationStep );
			painter->drawPixmap( -_pixmapActivity.width()/2, -_pixmapActivity.height()/2, _pixmapActivity );
		}
	}
	painter->restore();
}

QSize MainMenuDelegate::sizeHint( const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	return QSize( LeftPanelItemWidth, LeftPanelItemHeight );
}
