#include "sectioncontentitem.h"
#include "simplecontentitem.h"

SectionContentItem::SectionContentItem( const QString& text, SectionContentItem* parent ) : BaseContentItem() 
{
	_parent = parent;
	_text	= text;
}

SectionContentItem::~SectionContentItem()
{
	qDeleteAll(_childs);
}

//////////////////////////////////////////////////////////////////////////

QString SectionContentItem::text() const
{
	return _text;
}

void SectionContentItem::setText( const QString& text )
{
	Q_ASSERT(false);
	_text = text;
}

int SectionContentItem::childsCount() const
{
	return _childs.size(); 
}

BaseContentItem* SectionContentItem::child( int position ) const
{
	return _childs.at( position );
}

bool SectionContentItem::insertChild( int position )
{
	//if(position > _childs.size()) return false;
	
	_childs.insert( position, new SimpleContentItem( "_UNNAMED_", this) );
	return true;
}

bool SectionContentItem::removeChild( int position )
{
	//if(position > _childs.size()) return false;
	
	BaseContentItem* child = _childs.at( position );
	_childs.removeAt( position );
	delete child;
	return true;
}

BaseContentItem* SectionContentItem::takeChild( int position )
{
	BaseContentItem* child = _childs.at( position );
	_childs.removeAt( position );
	return child;
}

BaseContentItem* SectionContentItem::parent() const
{
	return _parent;
}

int SectionContentItem::sectionID() const
{
	return (_parent != NULL) ? _parent->sectionID() : -1;
}

int SectionContentItem::position() const
{
	return (_parent != NULL) ? _parent->childPosition( this ) : -1;
}

int SectionContentItem::childPosition( const BaseContentItem* child ) const
{
	return _childs.indexOf( const_cast<BaseContentItem*>( child ) );
}

int SectionContentItem::level() const
{
	return (_parent == NULL) ? 0 : _parent->level() + 1;
}

bool SectionContentItem::isHidden() const
{
	return false;
}

void SectionContentItem::setParent( BaseContentItem* )
{
}

bool SectionContentItem::addChild( BaseContentItem* item )
{
	item->setParent( this );
	_childs.prepend(item);
	return true;
}

//////////////////////////////////////////////////////////////////////////
