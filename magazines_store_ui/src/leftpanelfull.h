#ifndef LEFTPANELFULL_H
#define LEFTPANELFULL_H

#include <QWidget>

class ContentTreeView;
class QListView;
class QLabel;
class QToolButton;

class LeftPanelFull : public QWidget
{
	Q_OBJECT

public:
	LeftPanelFull( QWidget *parent, QWidget *parentForSelector );
	~LeftPanelFull();

signals:
	void hideButtonClicked( );

protected:
	void initUI( );

protected:
	void paintEvent(QPaintEvent *);

private:
	struct
	{		
		ContentTreeView*	contentTree;
		QListView*			contentList;
		QLabel*				lblDelimiter;
		QLabel*				lblLogo;
		QToolButton*		btnHide;
	}	_ui;

	QWidget*	_parentForSelector;
};

#endif // LEFTPANELFULL_H
