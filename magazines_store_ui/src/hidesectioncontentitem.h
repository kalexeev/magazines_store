#ifndef HIDESECTIONCONTENTITEM_H
#define HIDESECTIONCONTENTITEM_H

#include "sectioncontentitem.h"

class HideSectionContentItem : public SectionContentItem
{
public:
	HideSectionContentItem( SectionContentItem* parent );
	~HideSectionContentItem( );

	//////////////////////////////////////////////////////////////////////////
	virtual QSize sizeHint( ) const;

	virtual QString text( ) const;
	virtual void setText( const QString& );

	virtual bool isHidden() const;

};

#endif // HIDESECTIONCONTENTITEM_H
