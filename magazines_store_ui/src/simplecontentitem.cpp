#include "simplecontentitem.h"

SimpleContentItem::SimpleContentItem( const QString& text, BaseContentItem* parent ) : BaseContentItem()
{
	_parent		= parent;
	_text		= text; 
	_counter	= 0;
	_isLocked	= false;
}

SimpleContentItem::~SimpleContentItem()
{
}

//////////////////////////////////////////////////////////////////////////

QString SimpleContentItem::text() const
{
	return _text;
}

void SimpleContentItem::setText( const QString& text )
{
	_text = text;
}

int SimpleContentItem::childsCount() const
{
	return 0;
}

BaseContentItem* SimpleContentItem::child( int position ) const
{
	Q_ASSERT(false);
	return NULL;
}

bool SimpleContentItem::insertChild( int position )
{
	Q_ASSERT(false);
	return false;
}

bool SimpleContentItem::removeChild( int position )
{
	Q_ASSERT(false);
	return false;
}

BaseContentItem* SimpleContentItem::parent() const
{
	return _parent;
}

int SimpleContentItem::sectionID() const
{
	return _parent->sectionID();
}

int SimpleContentItem::position() const
{
	return _parent->childPosition( this );
}

int SimpleContentItem::childPosition( const BaseContentItem* ) const
{
	Q_ASSERT(false);
	return 0;
}

int SimpleContentItem::level() const
{
	return _parent->level() + 1;
}

bool SimpleContentItem::isHidden() const
{
	return _parent->isHidden();
}

bool SimpleContentItem::canDrag() const
{
	return _parent->acceptDrops();
}

bool SimpleContentItem::acceptDrops() const
{
	return false;
}

void SimpleContentItem::setParent( BaseContentItem* parent )
{
	_parent = parent;
}

int SimpleContentItem::counter() const
{
	return _counter;
}

void SimpleContentItem::setCounter( int value )
{
	_counter = value;
}

bool SimpleContentItem::isLocked() const
{
	return _isLocked;
}

void SimpleContentItem::setLocked( bool fLocked )
{
	_isLocked = fLocked;
}

bool SimpleContentItem::isDrawHideButton() const
{
	return _parent->isDrawHideButton();
}
