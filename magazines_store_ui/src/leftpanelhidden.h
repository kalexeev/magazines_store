#ifndef LEFTPANELHIDDEN_H
#define LEFTPANELHIDDEN_H

#include <QWidget>

class ButtonPanel;
class QToolButton;

class LeftPanelHidden : public QWidget
{
	Q_OBJECT

public:
	LeftPanelHidden(QWidget *parent);
	~LeftPanelHidden();

signals:
	void showButtonClicked( );

protected:
	void initUI( );

protected:
	void paintEvent(QPaintEvent *);

private:
	struct
	{		
		ButtonPanel*	buttonsMenu;
		QToolButton*	btnShow;
	}	_ui;
};

#endif // LEFTPANELHIDDEN_H
