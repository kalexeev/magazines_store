#ifndef METRICS_H
#define METRICS_H

enum LeftPanelMetrics
{
	LeftPanelItemPaddingLeft	=	5,
	LeftPanelItemHeight			=	22,
	LeftPanelItemWidth			=	170 + LeftPanelItemPaddingLeft,
	LeftPanelCounterWidth		=	30,
	LeftPanelTextIndent1		=	15,
	LeftPanelTextIndent2		=	20,
	LeftPanelTextIndent3		=	30
};

#endif