#include "contenttreeview.h"

#include <QItemSelection>
#include <QStyledItemDelegate>
#include <QEvent>
#include <QHelpEvent>
#include <QPixmap>
#include <QMouseEvent>
#include <QDebug>
#include <QPainter>
#include <QMimeData>
#include <QDrag>

#include "drawutils.h"
#include "contenttreedelegate.h"
#include "basecontentitem.h"
#include "metrics.h"

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

ContentTreeView::ContentTreeView(QWidget *parent, QWidget* parentSelector)
	: QTreeView(parent)
{
	_parentSelector = parentSelector;
	_selector = new ContentTreeViewSelector(parentSelector);
	_selector->setVisible(false);
	_selector->installEventFilter(this);

	setSelectionMode( QAbstractItemView::SingleSelection );
	setIndentation(0);
	setHeaderHidden(true);
	setRootIsDecorated(false);

	setItemDelegate( new ContentTreeDelegate(this) );
	setMouseTracking(true);
	
 	setDragDropMode(QAbstractItemView::InternalMove);
	setDropIndicatorShown(true);
	setAcceptDrops(true);
	setAnimated(true);
	setAutoScroll(true);
	
	viewport()->setFixedWidth( LeftPanelItemWidth + 5 );
	
	_dragSection	= -1;
	_dragIsHidden	= false;
}

ContentTreeView::~ContentTreeView()
{
}

//////////////////////////////////////////////////////////////////////////

void ContentTreeView::hideEvent( QHideEvent * )
{
	killTimer(_selectorTimer);
	_selector->hide();
}

void ContentTreeView::showEvent( QShowEvent * )
{
	_selectorTimer = startTimer(40);
}

void ContentTreeView::mousePressEvent( QMouseEvent *e )
{
	QTreeView::mousePressEvent( e );

	QModelIndex index = indexAt( e->pos() );
	if(index.isValid() && model()->hasChildren(index))
	{
		if(isExpanded(index))
 			collapse(index);
 		else
 			expand(index);
	}
}

void ContentTreeView::dragEnterEvent( QDragEnterEvent* e )
{
	if(e->mimeData()->hasFormat("application/basecontentitem"))
	{
		QByteArray encodedData = e->mimeData()->data("application/basecontentitem");
		QDataStream stream( &encodedData, QIODevice::ReadOnly );
		stream >> _dragSection >> _dragIsHidden;
		update();

		QTreeView::dragEnterEvent( e );
	}
	else
	{
		e->ignore();
	}
}

void ContentTreeView::dragMoveEvent( QDragMoveEvent* e )
{
	QModelIndex index = indexAt(e->pos());
	if( !index.isValid() )
	{
		e->ignore();
	}
	else
	{
		if(	(_dragSection == index.data( ContentTreeModel::SectionIdRole ).toInt())
			&& index.data( ContentTreeModel::IsHiddenRole) != _dragIsHidden)
		{
			e->accept();
		}
		else
		{
			e->ignore();
		}
	}
}

void ContentTreeView::dropEvent( QDropEvent* e )
{
	QModelIndex index = indexAt(e->pos());
	if( !index.isValid() )
	{
		e->ignore();
	}
	else
	{
 		if(_dragSection == index.data( ContentTreeModel::SectionIdRole ).toInt())
		{
			QTreeView::dropEvent( e );
			if( !e->isAccepted() ) e->setDropAction(Qt::IgnoreAction);
		}
		else
			e->ignore();
	}

	_dragSection = -1;
	updateSelector( e->pos() );
}

void ContentTreeView::drawRow( QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const
{
	QTreeView::drawRow(painter, option, index);

	if( (_dragSection != -1) &&
		!_dragIsHidden && (_dragSection == index.data(ContentTreeModel::SectionIdRole)) &&
		(index.data(ContentTreeModel::IsHiddenRole).toBool() != _dragIsHidden) &&
		(index.data(ContentTreeModel::ContentItemTypeRole) == BaseContentItem::tSection) )
	{
		painter->save();
	
		QRect rectSelection =  option.rect.adjusted(0,0,-1,-1);
		painter->setClipRect( rectSelection );
		rectSelection.setWidth( LeftPanelItemWidth );
		DrawUtils::drawSelectionFrame( painter, rectSelection );

		painter->restore();
	}
}

void ContentTreeView::startDrag( Qt::DropActions supportedActions )
{
	QModelIndexList indexes = this->selectionModel()->selectedIndexes();
	if (indexes.count() > 0) {
 		QMimeData *data = model()->mimeData(indexes);
 		if (!data)
 			return;
 		QRect rect = _selector->rect();
		rect.moveTopLeft(QPoint(0,0));
 		QPixmap pixmap(rect.size());
		pixmap.fill(Qt::transparent);
	
		QPainter painter(&pixmap);
		painter.setOpacity(0.8);
		_selector->render(&painter, QPoint(), QRegion(), 0);

		QDrag *drag = new QDrag(this);
		drag->setPixmap(pixmap);
		drag->setMimeData(data);
		drag->setHotSpot( _selector->mapFromGlobal(QCursor::pos()) );
		drag->exec(supportedActions, Qt::MoveAction);

		_dragSection = -1;
		update();
 	}
}

void ContentTreeView::drawBranches( QPainter *painter, const QRect &rect, const QModelIndex &index ) const
{
	/* NO BRANCH INDICATOR */
}

void ContentTreeView::updateSelector( const QPoint& posCursor )
{
	static QPoint sPrevPos = QPoint();

	if(_dragSection == -1 && (sPrevPos != posCursor) ) // If not drag
	{
		QModelIndex index = indexAt( posCursor );
		if(index.isValid() && (index.flags() & Qt::ItemIsSelectable))
		{
			QRect rectItem = visualRect(index);
			QRect rVP = this->rect();
			if(rVP.contains( viewport()->mapToParent(rectItem.topLeft())))
			{
				_selector->setupForIndex( index, isExpanded(index) );
				_selector->move( viewport()->mapTo( _selector->parentWidget(), rectItem.topLeft() ) );
				_selector->show();
			}
			else
			{
				_selector->hide();
			}
		}
		else
		{
			_selector->hide();
		}
	}
}

void ContentTreeView::timerEvent( QTimerEvent* e )
{
	if(e->timerId() == _selectorTimer)
	{
		updateSelector( mapFromGlobal(QCursor::pos()) );
	}
	QTreeView::timerEvent(e);
}

void ContentTreeView::setModel( QAbstractItemModel *model )
{
	connect( _selector, SIGNAL(sigHideItem(int,const QString&)), model, SIGNAL(sigHideItem(int,const QString&)) );
	QTreeView::setModel( model );
}
