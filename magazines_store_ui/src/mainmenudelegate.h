#ifndef MAINMENUDELEGATE_H
#define MAINMENUDELEGATE_H

#include <QStyledItemDelegate>

class MainMenuDelegate : public QStyledItemDelegate
{
	Q_OBJECT

public:
	enum Roles
	{
		Counter1Role = Qt::UserRole,
		Counter2Role,
		ActivityRole,
		RoleMAX
	};

	MainMenuDelegate(QObject *parent);
	~MainMenuDelegate();


	void paint( QPainter* painter, const QStyleOptionViewItem & option, const QModelIndex & index ) const;
	QSize sizeHint ( const QStyleOptionViewItem & option, const QModelIndex & index ) const;

public slots:
	void setAnimationStep( int step ) { _animationStep = step; }

private:
	QPixmap		_pixmapActivity;	
	int			_animationStep;
};

#endif // MAINMENUDELEGATE_H
