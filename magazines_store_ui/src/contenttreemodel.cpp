#include "contenttreemodel.h"
#include "topsectioncontentitem.h"
#include "spacercontentitem.h"

#include <QMimeData>
#include <QDebug>

ContentTreeModel::ContentTreeModel(QObject *parent)
	: QAbstractItemModel(parent)
{
	initRootSection( );

	_fonts[ fontTopItem ] = QFont("Calibri");
	_fonts[ fontTopItem ].setPixelSize( 15 );
	_fonts[ fontTopItem ].setBold( true );

	_fonts[ fontSubItem ] = QFont("Calibri");
	_fonts[ fontSubItem ].setPixelSize( 14 );
	_fonts[ fontSubItem ].setBold( false );

	_textColors[ textClrVisibleItem ] = QColor( 77, 77, 77 );
	_textColors[ textClrHiddenItem ]  = QColor( 122, 120, 115 );
}

ContentTreeModel::~ContentTreeModel()
{
	qDeleteAll(_rootItems);
}

int ContentTreeModel::columnCount(const QModelIndex &parent) const
{
	return 1;
}

QVariant ContentTreeModel::data(const QModelIndex &index, int role) const
{
	if (!index.isValid())
		return QVariant();

	if (role == Qt::DisplayRole)
	{
		return itemFromIndex(index)->text();
	}
	else if(role == Qt::FontRole)
	{
		return (itemFromIndex(index)->level()) == 0 ? _fonts[ fontTopItem ] : _fonts[ fontSubItem ];
	}
	else if(role == Qt::TextColorRole)
	{
		return 	(itemFromIndex(index)->isHidden() && (itemFromIndex(index)->type() == BaseContentItem::tSimpleItem)) ? 
					_textColors[ textClrHiddenItem ] : 
					_textColors[ textClrVisibleItem ];
	}
	else if(role == Qt::SizeHintRole)
	{
		return itemFromIndex(index)->sizeHint();
	}
	else if(role == SectionIdRole)
	{
		return itemFromIndex( index )->sectionID();
	}
	else if(role == ContentItemTypeRole)
	{
		return static_cast<int>(itemFromIndex( index )->type());
	}
	else if(role == LevelRole)
	{
		return itemFromIndex( index )->level();
	}
	else if(role == CounterRole)
	{
		return itemFromIndex(index)->isHidden() ? 0 : itemFromIndex( index )->counter();
	}
	else if(role == IsHiddenRole)
	{
		return itemFromIndex( index )->isHidden();
	}
	else if(role == IsLockedRole)
	{
		return itemFromIndex( index )->isLocked();
	}
	else if(role == IsDrawHideButtonRole)
	{
		return itemFromIndex( index )->isDrawHideButton();
	}	

 	return QVariant();
}

Qt::ItemFlags ContentTreeModel::flags(const QModelIndex &index) const
{
	if (!index.isValid())
		return 0;

	Qt::ItemFlags flags = Qt::ItemIsEnabled;

	BaseContentItem* item = static_cast<BaseContentItem*> (index.internalPointer());
	if( item->level() > 0 ) {
		flags |= Qt::ItemIsSelectable;
	}
	if( item->canDrag() ) { 
		flags |= Qt::ItemIsDragEnabled; 
	}
	if( item->acceptDrops() ) { 
		flags |= Qt::ItemIsDropEnabled; 
	}
	return flags;
}

QModelIndex ContentTreeModel::index(int row, int column, const QModelIndex &parent)	const
{
	if (!hasIndex(row, column, parent))
		return QModelIndex();

	if (!parent.isValid()) // ROOT ITEMS
	{
		if(row < _rootItems.size())
			return createIndex(row, column, _rootItems.at(row));
		else
			return QModelIndex();
	}
	else
	{
 		BaseContentItem* parentItem = static_cast<BaseContentItem*>(parent.internalPointer());
 		BaseContentItem* childItem  = parentItem->child(row);
 		if (childItem)
 			return createIndex(row, column, childItem);
	}
	return QModelIndex();
}

QModelIndex ContentTreeModel::parent(const QModelIndex &index) const
{
	if (!index.isValid())
		return QModelIndex();

	BaseContentItem *childItem  = static_cast< BaseContentItem* >(index.internalPointer());
	BaseContentItem *parentItem = childItem->parent();

	if (parentItem == NULL)
		return QModelIndex();

	int position = 0;
	if(parentItem->level() == 0)
		position = _rootItems.indexOf(parentItem);
	else
		position = parentItem->position();
	return createIndex( position, 0, parentItem );
}

int ContentTreeModel::rowCount(const QModelIndex &parent) const
{
	if (parent.column() > 0)
		return 0;

	BaseContentItem *parentItem;
	if (!parent.isValid())
		return _rootItems.size();
	else
		parentItem = static_cast< BaseContentItem* >(parent.internalPointer());

	return parentItem->childsCount();
}

void ContentTreeModel::initRootSection()
{
	_sections[SectionAll]		= new TopSectionContentItem(tr("SECTION1"), SectionAll, true);
	_sections[SectionPublish]	= new TopSectionContentItem(tr("SECTION2"), SectionPublish, true);
	_sections[SectionRecent]	= new TopSectionContentItem(tr("SECTION3"), SectionRecent, false);

	_rootItems.append( new SpacerContentItem( 10 ) );
	_rootItems.append( _sections[SectionAll] );
	_rootItems.append( new SpacerContentItem( 18 ) );
	_rootItems.append( _sections[SectionPublish] );
	_rootItems.append( new SpacerContentItem( 18 ) );
	_rootItems.append( _sections[SectionRecent] );
}

void ContentTreeModel::addSectionItem( eRootSectionsID section, const QString& strItem, int counter /*= 0*/, bool isLocked /*= false*/ )
{
	if(_sections[section]->insertChild( 0 ))
	{
		_sections[section]->child( 0 )->setText( strItem );
		_sections[section]->child( 0 )->setCounter( counter );
		_sections[section]->child( 0 )->setLocked( isLocked );
	}
}

Qt::DropActions ContentTreeModel::supportedDropActions() const
{
	return Qt::MoveAction;
}

bool ContentTreeModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
	if(!index.isValid()) return false;

	bool result = false;
	if(role == Qt::DisplayRole)
	{
		BaseContentItem* item = static_cast< BaseContentItem* > (index.internalPointer());
		item->setText(value.toString());
		result = true;
	}
	else if(role == CounterRole)
	{
		BaseContentItem* item = static_cast< BaseContentItem* > (index.internalPointer());
		item->setCounter(value.toInt());
		result = true;
	}
 	if (result) emit dataChanged(index, index);

	return true;
}

bool ContentTreeModel::dropMimeData( const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent )
{
	if(!data->hasFormat("application/basecontentitem")) 
		return false;

	if(!parent.isValid())
		return false;

	if(row > rowCount(parent) || row == -1)
		row = 0;
 	
	int dropSection;
	bool dropIsHidden;
	QString dropText;
	QByteArray encodedData = data->data("application/basecontentitem");
	QDataStream stream( &encodedData, QIODevice::ReadOnly );
	stream >> dropSection >> dropIsHidden >> dropText;

	
// 	
// 	BaseContentItem* item = _sections[dropSection]->findSubItem(dropText);
// 	if( item == NULL ) 
// 		return false;
// 		
// 	TopSectionContentItem* topSectionItem = _sections[dropSection];
// 
// 	QModelIndex source;
// 	QModelIndex dest;
// 	if(item->isHidden())
// 	{
// 		source	= createIndex(topSectionItem->hideSection()->position(), 0, topSectionItem->hideSection());
// 		dest	= createIndex(_rootItems.indexOf(topSectionItem), 0, topSectionItem);
// 	}
// 	else
// 	{
// 		source	= createIndex(_rootItems.indexOf(topSectionItem),0, topSectionItem);
// 		dest	= createIndex(topSectionItem->hideSection()->position(), 0, topSectionItem->hideSection());
// 	}
// 	
// 	bool result = false;
// 	int itemPos = item->position();
// 	beginMoveRows( source, itemPos, itemPos, dest, 0 );
// 	if(item->isHidden())
// 		result = _sections[dropSection]->showChild(item->position());
// 	else
// 		result = _sections[dropSection]->hideChild(item->position());
// 	endMoveRows();

	return setItemVisibility( static_cast<eRootSectionsID>(dropSection), dropText, dropIsHidden );
}


bool ContentTreeModel::setItemVisibility( eRootSectionsID section, const QString& strItem, bool isVisible )
{
	BaseContentItem* item = _sections[section]->findSubItem(strItem);
	if( item == NULL ) 
		return false;
	if(item->isHidden() != isVisible)
		return true;

	TopSectionContentItem* topSectionItem = _sections[section];

	QModelIndex source;
	QModelIndex dest;
	if( isVisible )
	{
		source	= createIndex(topSectionItem->hideSection()->position(), 0, topSectionItem->hideSection());
		dest	= createIndex(_rootItems.indexOf(topSectionItem), 0, topSectionItem);
	}
	else
	{
		source	= createIndex(_rootItems.indexOf(topSectionItem),0, topSectionItem);
		dest	= createIndex(topSectionItem->hideSection()->position(), 0, topSectionItem->hideSection());
	}

	bool result = false;
	int itemPos = item->position();
	beginMoveRows( source, itemPos, itemPos, dest, 0 );
	if(item->isHidden())
		result = _sections[section]->showChild(item->position());
	else
		result = _sections[section]->hideChild(item->position());
	endMoveRows();

	return result;
}


bool ContentTreeModel::insertRows( int position, int rows, const QModelIndex &parent /*= QModelIndex()*/ )
{
	if( rows != 1 ) return false;

	BaseContentItem* parentItem = NULL;
	if(parent.isValid())
	{
		parentItem = static_cast<BaseContentItem*> (parent.internalPointer());
	}
	else
	{
		if(position >= SectionMAX) return false;
		parentItem = _sections[position];
	}
	bool success;
	beginInsertRows(parent, position, position);
	success = parentItem->insertChild(position);
	endInsertRows();

	return success;
}

bool ContentTreeModel::removeRows( int position, int rows, const QModelIndex &parent /*= QModelIndex()*/ )
{
	if( rows != 1 ) return false;

	BaseContentItem* parentItem = NULL;
	if(parent.isValid())
	{
		parentItem = static_cast<BaseContentItem*> (parent.internalPointer());
	}
	else
	{
		if(position >= SectionMAX) return false;
		parentItem = _sections[position];
	}
	bool success;
	beginRemoveRows(parent, position, position);
	success = parentItem->removeChild(position);
	endRemoveRows();

	return success;
}

QMap<int, QVariant> ContentTreeModel::itemData( const QModelIndex &index ) const
{
	QMap<int, QVariant> roles;
	for (int i = 0; i < RoleMAX; ++i) {
		QVariant variantData = data(index, i);
		if (variantData.isValid())
			roles.insert(i, variantData);
	}
	return roles;
}

BaseContentItem* ContentTreeModel::itemFromIndex( const QModelIndex& index ) const
{
	if(index.isValid())
	{
		BaseContentItem *item = static_cast<BaseContentItem*>(index.internalPointer());
		return item;
	}
	return NULL;
}

QMimeData * ContentTreeModel::mimeData( const QModelIndexList & indexes ) const
{
	QMimeData* data = QAbstractItemModel::mimeData( indexes );

	QByteArray encodedData;
	QDataStream stream( &encodedData, QIODevice::WriteOnly );
	stream	<< indexes.at(0).data(SectionIdRole).toInt() 
			<< indexes.at(0).data(IsHiddenRole).toBool()
			<< indexes.at(0).data(Qt::DisplayRole).toString();
	data->setData( "application/basecontentitem", encodedData );

	return data;
}

void ContentTreeModel::onHideItem( int sectionId, const QString& itemText )
{
	setItemVisibility( static_cast<eRootSectionsID> (sectionId), itemText, false );
}

//////////////////////////////////////////////////////////////////////////
