<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>ButtonPanel</name>
    <message>
        <location filename="src/buttonpanel.cpp" line="25"/>
        <source>Tooltip %0</source>
        <translatorcomment>Подсказка %1</translatorcomment>
        <translation></translation>
    </message>
</context>
<context>
    <name>ContentTreeModel</name>
    <message>
        <location filename="src/contenttreemodel.cpp" line="167"/>
        <source>SECTION1</source>
        <translation>ВСЕ НОМЕРА</translation>
    </message>
    <message>
        <location filename="src/contenttreemodel.cpp" line="168"/>
        <source>SECTION2</source>
        <translation>ИЗДАНИЯ</translation>
    </message>
    <message>
        <location filename="src/contenttreemodel.cpp" line="169"/>
        <source>SECTION3</source>
        <translation>НЕДАВНО ЗАКРЫТЫЕ</translation>
    </message>
</context>
<context>
    <name>LeftPanelFull</name>
    <message>
        <location filename="src/leftpanelfull.cpp" line="44"/>
        <source>SubSection1_1</source>
        <translation>Политика</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="45"/>
        <location filename="src/leftpanelfull.cpp" line="59"/>
        <source>SubSection1_2</source>
        <translation>Бизнес</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="46"/>
        <source>SubSection1_3</source>
        <translation>Спорт</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="47"/>
        <source>SubSection1_4</source>
        <translation>18+</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="49"/>
        <source>SubSection2_1</source>
        <translation>Billboard</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="50"/>
        <source>SubSection2_2</source>
        <translation>Time</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="51"/>
        <source>SubSection2_3</source>
        <translation>RollingStones</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="53"/>
        <source>SubSection3_1</source>
        <translation>2011-11 Details</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="54"/>
        <source>SubSection3_2</source>
        <translation>2011-11 Vanity Fair</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="55"/>
        <source>SubSection3_3</source>
        <translation>2011-11 Metro Beauty</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="56"/>
        <source>SubSection3_4</source>
        <translation>2011-11 Красота &amp; здоровье</translation>
    </message>
    <message>
        <location filename="src/leftpanelfull.cpp" line="57"/>
        <source>SubSection3_5</source>
        <translation>2011-11 Седьмой континент</translation>
    </message>
</context>
<context>
    <name>MainMenuList</name>
    <message>
        <location filename="src/mainmenulist.cpp" line="68"/>
        <source>Table of contents</source>
        <translation>Оглавление</translation>
    </message>
    <message>
        <location filename="src/mainmenulist.cpp" line="69"/>
        <source>Favourites</source>
        <translation>Избранное</translation>
    </message>
    <message>
        <location filename="src/mainmenulist.cpp" line="70"/>
        <source>Notes</source>
        <translation>Заметки</translation>
    </message>
    <message>
        <location filename="src/mainmenulist.cpp" line="71"/>
        <source>Events</source>
        <translation>События</translation>
    </message>
    <message>
        <location filename="src/mainmenulist.cpp" line="72"/>
        <source>Downloads</source>
        <translation>Загрузки</translation>
    </message>
    <message>
        <location filename="src/mainmenulist.cpp" line="73"/>
        <source>Shopping</source>
        <translation>Покупки</translation>
    </message>
    <message>
        <location filename="src/mainmenulist.cpp" line="74"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="src/hidesectioncontentitem.cpp" line="19"/>
        <source>%0 hidden</source>
        <translation>%0 скрыто</translation>
    </message>
</context>
</TS>
