import QtQuick 1.1

Item {
    id: scrollBar

    property real position
    property real pageSize
    property int orientation : Qt.Vertical

    signal sigPositionChangedInternal (real internalPos)

    visible: pageSize < 1.0

    MouseArea {
        property bool   scrolling : false
        property int    pressY: 0
        property real   pressPos: 0

        anchors.fill: parent
        acceptedButtons: Qt.LeftButton

        onPressed : {
			pressY = mouseY
            if( parent.childAt(mouseX, mouseY) === handle ) {
                scrolling = true
                pressPos = position
            }
            else if (pageSize < 1.0){
                var tmpPosition = Math.max(0, Math.min( 1.0  - pageSize, 
					pressY / scrollBar.height - (handle.height * 0.5) / scrollBar.height ));

                if(tmpPosition != scrollBar.position)
                    sigPositionChangedInternal( tmpPosition )
            }
        }

        onReleased : {
             scrolling = false
        }

        onPositionChanged : {
            if( scrolling ) {
                var tmpPosition = Math.max(0, Math.min( 1.0  - pageSize, 
					pressPos + (mouseY - pressY) / scrollBar.height ));
				
                if(tmpPosition != scrollBar.position)
                    sigPositionChangedInternal( tmpPosition )
            }
        }
    }

    Rectangle {
        id: background
        anchors.horizontalCenter: handle.horizontalCenter
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: parent.width * 0.6

        radius: orientation == Qt.Vertical ? (width/2 - 1) : (height/2 - 1)

        border.color: "#9948535b"
        border.width: 1
        color: "#16191c"
    }

    Rectangle {
        id: handle
        x: orientation == Qt.Vertical ? 0 : (scrollBar.position * scrollBar.width)
        y: orientation == Qt.Vertical ? (scrollBar.position * scrollBar.height) : 0
        width: orientation == Qt.Vertical ? parent.width : (scrollBar.pageSize * scrollBar.width)
        height: orientation == Qt.Vertical ? (scrollBar.pageSize * scrollBar.height) : parent.height
        radius: orientation == Qt.Vertical ? (width/2 - 1) : (height/2 - 1)

        border.color: "#80000000"
        border.width: 1
        color: "#b8b8b8"
    }

}
