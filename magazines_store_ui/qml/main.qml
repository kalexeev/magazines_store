import QtQuick 1.1

Item {
    width: 800
    height: 600

    Rectangle {
        id: preview

        width: 184
        height: 222

        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 10
        anchors.topMargin: 10
        border.color: "#cdd907"

        Image {
            id: imgGlass

            z: 51
            x: preview.x + 1
            y: preview.y + 1

            source: "./pngs/preview_top_glass.png"
        }
    }

    NewsSection {
        id: newsSection
        anchors.right: parent.right
        anchors.left: preview.right
        anchors.top: parent.top
        anchors.bottom: preview.bottom
        anchors.leftMargin: 20
        anchors.rightMargin: 15
        anchors.topMargin: 10
    }

    SearchField {
        id: search
        focus: true

        height: 30
        width: 200
        anchors.top: preview.bottom
        anchors.right: parent.right
        anchors.topMargin: 10
        anchors.rightMargin: 15
    }

	Item
	{
		anchors.left: parent.left
		anchors.top: search.bottom
		anchors.right: parent.right
		anchors.bottom: parent.bottom
		anchors.leftMargin: 10
		anchors.topMargin: 10
		anchors.rightMargin: 0
		anchors.bottomMargin: 10

		GridView {
			id: gridViewItems
			anchors.left: parent.left
			anchors.top: parent.top
			anchors.right: verticalScrollBar.left
			anchors.bottom: parent.bottom
			focus: true
			clip : true

			cellHeight: 250
			cellWidth:  160

			boundsBehavior: Flickable.StopAtBounds

			function onVScrollBarPosChanged( pos ) {
				gridViewItems.contentY = gridViewItems.contentHeight * pos
			}

			 model: ListModel {
				ListElement {
					image: "./pngs/mag1.png"
					title: "Очень длинное название журнала"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag2.png"
					title: "Журнал 1"
					isNew: true
				}

				ListElement {
					image: "./pngs/mag3.png"
					title: "Журнал 2"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag4.png"
					title: "Журнал 3"
					isNew: false
				}
						  ListElement {
					image: "./pngs/mag1.png"
					title: "Очень длинное название журнала"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag2.png"
					title: "Журнал 1"
					isNew: true
				}

				ListElement {
					image: "./pngs/mag3.png"
					title: "Журнал 2"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag4.png"
					title: "Журнал 3"
					isNew: false
				}
						  ListElement {
					image: "./pngs/mag1.png"
					title: "Очень длинное название журнала"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag2.png"
					title: "Журнал 1"
					isNew: true
				}

				ListElement {
					image: "./pngs/mag3.png"
					title: "Журнал 2"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag4.png"
					title: "Журнал 3"
					isNew: false
				}
						  ListElement {
					image: "./pngs/mag1.png"
					title: "Очень длинное название журнала"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag2.png"
					title: "Журнал 1"
					isNew: true
				}

				ListElement {
					image: "./pngs/mag3.png"
					title: "Журнал 2"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag4.png"
					title: "Журнал 3"
					isNew: false
				}
						  ListElement {
					image: "./pngs/mag1.png"
					title: "Очень длинное название журнала"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag2.png"
					title: "Журнал 1"
					isNew: true
				}

				ListElement {
					image: "./pngs/mag3.png"
					title: "Журнал 2"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag4.png"
					title: "Журнал 3"
					isNew: false
				}
						  ListElement {
					image: "./pngs/mag1.png"
					title: "Очень длинное название журнала"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag2.png"
					title: "Журнал 1"
					isNew: true
				}

				ListElement {
					image: "./pngs/mag3.png"
					title: "Журнал 2"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag4.png"
					title: "Журнал 3"
					isNew: false
				}
						  ListElement {
					image: "./pngs/mag1.png"
					title: "Очень длинное название журнала"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag2.png"
					title: "Журнал 1"
					isNew: true
				}

				ListElement {
					image: "./pngs/mag3.png"
					title: "Журнал 2"
					isNew: false
				}

				ListElement {
					image: "./pngs/mag4.png"
					title: "Журнал 3"
					isNew: false
				}
			}
			delegate: MagazineCtrl {
				isNewMagazine: isNew
				posterImageSource: image
				magazineTitle: title
				magazineNumber: "Номер ###"
			}

			// Only show the scrollbars when the view is moving.
			states: State {
				name: "ShowBars"
				when: gridViewItems.movingVertically
				PropertyChanges { target: verticalScrollBar; opacity: 1 }
			}

			transitions: Transition {
				NumberAnimation { properties: "opacity"; duration: 400 }
			}
		}

		// Attach scrollbars to the right and bottom edges of the view.
		ScrollBar {
			id: verticalScrollBar
			width: 10;
		
			anchors.top: parent.top
			anchors.bottom: parent.bottom
			anchors.right: parent.right
			anchors.rightMargin: 5
			anchors.leftMargin: 5
			anchors.topMargin: 10
			anchors.bottomMargin: 10

			orientation: Qt.Vertical
			position: gridViewItems.visibleArea.yPosition
			pageSize: gridViewItems.visibleArea.heightRatio

			Component.onCompleted: {
				verticalScrollBar.sigPositionChangedInternal.connect(gridViewItems.onVScrollBarPosChanged)
			}
		}
	}
}
