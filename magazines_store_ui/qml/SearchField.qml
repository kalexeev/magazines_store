import QtQuick 1.0

FocusScope {
    id: focusScope

    BorderImage {
        source: "./pngs/find_bg.png"
        width: parent.width; height: parent.height
        border { left: 4; top: 4; right: 4; bottom: 4 }
    }
/*
    BorderImage {
        source: "images/lineedit-bg-focus.png"
        width: parent.width; height: parent.height
        border { left: 4; top: 4; right: 4; bottom: 4 }
        visible: parent.activeFocus ? true : false
    }
*/
    Text {
        id: typeSomething

        anchors { left: imgIcon.right; leftMargin: 8; right: clear.left; rightMargin: 8; verticalCenter: parent.verticalCenter }

        verticalAlignment: Text.AlignVCenter
        text: "Найти"

        font.family: "Calibri"
        font.pixelSize: 14
        color: "#7c8891"
    }

    MouseArea {
        anchors.fill: parent
        onClicked: { focusScope.focus = true; textInput.openSoftwareInputPanel(); }
    }

    TextInput {
        id: textInput
        anchors { left: imgIcon.right; leftMargin: 8; right: clear.left; rightMargin: 8; verticalCenter: parent.verticalCenter }
        focus: true
        selectByMouse: true

        font.family: "Calibri"
        font.pixelSize: 14
        color: "#7c8891"
    }

    Image {
        id: imgIcon
        anchors { left: parent.left; leftMargin: 8; verticalCenter: parent.verticalCenter }
        source: "./pngs/find_icon.png"
    }

    Image {
        id: clear
        anchors { right: parent.right; rightMargin: 8; verticalCenter: parent.verticalCenter }
        source: "./pngs/find_clear.png"
        opacity: 0

        MouseArea {
            anchors.fill: parent
            onClicked: { textInput.text = ''; focusScope.focus = true; textInput.openSoftwareInputPanel(); }
        }
    }

    states: State {
        name: "hasText"; when: textInput.text != ''
        PropertyChanges { target: typeSomething; opacity: 0 }
        PropertyChanges { target: clear; opacity: 1 }
    }

    transitions: [
        Transition {
            from: ""; to: "hasText"
            NumberAnimation { exclude: typeSomething; properties: "opacity" }
        },
        Transition {
            from: "hasText"; to: ""
            NumberAnimation { properties: "opacity" }
        }
    ]
}
