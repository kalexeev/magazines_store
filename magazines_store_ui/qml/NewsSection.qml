import QtQuick 1.1

Item {
    id: newsSection

    property int pageCount   : 4
    property int pageCurrent : 0

    MouseArea {
        id: ma
        hoverEnabled: true
        anchors.fill: parent

        onPressed: {
            if( newsSection.childAt(mouse.x, mouse.y) === btnNextNews ) {
                pageCurrent = (pageCurrent + 1) % pageCount
            }
        }
    }

    BorderImage {

        anchors.fill: parent
        source: "./pngs/news_bg.png"
        border.left: 5; border.top: 5
        border.right: 5; border.bottom: 5
        horizontalTileMode: BorderImage.Repeat
        verticalTileMode: BorderImage.Repeat
    }

    Rectangle {
        id: poster

        width: 140
        height: 184

        anchors.left: parent.left
        anchors.top: parent.top
        anchors.leftMargin: 20
        anchors.topMargin: 10
        border.color: "#514d4a"
    }

    Image {
        id: btnNextNews

        source: "./pngs/news_glass_btn.png"
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        anchors.rightMargin: 10
        visible: ma.containsMouse

        z : 100
    }

    Image {
        id: imgPages

        fillMode: Image.TileHorizontally
        source: "./pngs/dot_page.png"
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottomMargin: 8

        visible: pageCount > 1
        width: sourceSize.width * pageCount
    }

    Image {
        source: "./pngs/dot_page_current.png"
        anchors.top: imgPages.top

        visible: pageCount > 1
        x: imgPages.x + pageCurrent * sourceSize.width
    }

    Column {
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.left: poster.right
        anchors.bottom: parent.bottom
        anchors.rightMargin: 10
        anchors.leftMargin: 10
        anchors.topMargin: 10

        Text {
            id: textTitle1

            text: "Анонс журнала"
            horizontalAlignment: Text.AlignLeft
            font.family: "Arial"
            font.pixelSize: 22
            font.bold: false
            color: "#ffffff"
        }
        Text {
            id: textTitle2

            text: "Title 2"
            horizontalAlignment: Text.AlignLeft
            font.family: "Arial"
            font.pixelSize: 22
            font.bold: true
            color: "#ffffff"
        }
        Text {
            id: textMain

            wrapMode: Text.Wrap
            width: parent.width
            text: "<b>Здесь будет текст новостей.</b><br /> Здесь будет текст новостей. Здесь будет текст новостей. Здесь будет текст новостей. Здесь будет текст новостей. Здесь будет текст новостей. "
            horizontalAlignment: Text.AlignJustify
            font.family: "Arial"
            font.pixelSize: 11
            font.bold: false
            color: "#aeb9c2"
        }
    }
}

