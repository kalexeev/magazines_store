import QtQuick 1.1

Item {
    id: control

    width:  160
    height: 250

    property string posterImageSource
    property string magazineTitle
    property string magazineNumber
    property bool isNewMagazine : false


    Rectangle {
        id: selection

        visible: ma.containsMouse
        border.color: "#6c7c89"
        border.width: 2
        radius: 2

        x: 1
        y: 1
        width: parent.width - border.width
        height: parent.height - border.width

        color: "transparent"
        z: 1
    }

    MouseArea {
        id: ma

        hoverEnabled: true
        anchors.fill: parent
        onEntered : {
            console.log("onEntered")
        }
        onExited : {
            console.log("onExited")
        }
        onPressed: {
            console.log("onPressed")
        }
        onReleased : {
            console.log("onReleased")
        }
    }

    Item {
        width: parent.width - 10
        height: parent.height - 10

        anchors.centerIn: parent

        Item {
            id: poster

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.bottom: textTitle.top

            Image {
                id: imgPocket

                z: 80
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                source: "./pngs/mag_pocket.png"
            }

            Image {
                id: imgNew

                visible: isNewMagazine
                z: 100
                anchors.topMargin: -4
                anchors.leftMargin: -4
                anchors.top: imgPoster.top
                anchors.left: imgPoster.left
                source: "./pngs/mag_new.png"
            }

            Image {
                id: imgGlass

                z: 51

                x: imgPoster.x
                y: imgPoster.y - 1

                source: "./pngs/mag_top_glass.png"
            }

            Image {
                id: imgPoster

                z: 50

                width: 140
                fillMode: Image.PreserveAspectCrop

                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: imgPocket.bottom
                anchors.bottomMargin: 6
                source: posterImageSource
            }
        }

        Text {
            id: textTitle

            height: 15

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: textNumber.top

            text: magazineTitle
            horizontalAlignment: Text.AlignHCenter
            elide:  Text.ElideRight
            font.family: "Arial"
            font.pixelSize: 12
            font.bold: true
            color: "#c8d5df"
        }

        Text {
            id: textNumber

            height: 15

            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            text: magazineNumber
            horizontalAlignment: Text.AlignHCenter
            elide:  Text.ElideRight
            font.family: "Arial"
            font.pixelSize: 11
            color: "#c8d5df"
        }
    }


}
